// initialize datatable on adalbums page
   $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "drawCallback": function( settings ) {

            // popover
            $('[data-toggle="popover"]').popover({
                placement : 'top',
                trigger : 'hover'
            }); 
            
            //delete confirm popup
            $('.delete').click(function(){
            var id= $(this).attr('id');
            var href= $(this).attr('data-href');
            $('.del-confirm').attr('href',href+id);
            });
            //delete confirm popup
            
            // edit album function in same field 
            $('.edit-btn').click(function(){
                var id= $(this).attr('id');
                $.ajax({
                    url: "views/editalbumajax.php",
                    type: "POST",
                    data: {edit : id},
                    beforeSend: function(){
                         $('.cssload-whirlpool1').show();
                         $('.addform').fadeTo(0,0.1);
                     },
                    success: function(data) {
                     $('.form-box').html(data);
                     $('.cssload-whirlpool1').delay(2000).fadeOut();
                     $('.addform').delay(2000).fadeTo(0, 1);
                     disablesubmit();
                    window.scrollTo(0,0);
                    }
                });
    });
    
    //call to localdate function

        },
    });
    
    // popover
    $('[data-toggle="popover"]').popover({
        placement : 'top',
        trigger : 'hover'
    }); 
    // popover
    
    //deleteimagefilter('#filter'); function
    $('.delete').click(function(){
    var id= $(this).attr('id');
    var href= $(this).attr('data-href');
    $('.del-confirm').attr('href',href+id);
    });
    
    // cancle update
    $('.cancle').click(function(){
        //get all albums 
        $.ajax({
                url: "views/allalbumsajax.php",
                type: "POST",
                data: 'data',
                success: function(data) {
                $("#result").html('');
                 $('.albumdata').html(data);
                }
            });
        //get all albums
    });
    // cancle update
    
    // edit album function in same field 
    $('.edit-btn').click(function(){
        var id= $(this).attr('id');
        $.ajax({
            url: "views/editalbumajax.php",
            type: "POST",
            data: {edit : id},
            beforeSend: function(){
                 $('.cssload-whirlpool1').show();
                 $('.addform').fadeTo(0,0.1);
             },
            success: function(data) {
             $('.form-box').html(data);
             $('.cssload-whirlpool1').delay(2000).fadeOut();
             $('.addform').delay(2000).fadeTo(0, 1);
             disablesubmit();
            window.scrollTo(0,0);
            }
        });
    });
    
    //call to localdate function
   //getting local datetime
    setInterval(localdate(), 1000);
    
    
    
    
    
   