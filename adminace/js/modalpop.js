
// Set timeout variables.
var timoutWarning = 2160000; // Display warning in 15Mins.
var timoutNow = 2160000; // Timeout in 2 mins.
var logoutUrl = 'logout.php';
var warningTimer;
var timeoutTimer;

// Start timers.
function StartTimers() {
 
    warningTimer = setTimeout("IdleWarning()", timoutWarning);
    timeoutTimer = setTimeout("IdleTimeout()", timoutNow);
}

// Reset timers.
function ResetTimers() {
    clearTimeout(warningTimer);
    clearTimeout(timeoutTimer);
    StartTimers();
    /*$("#logout_popup").modal('hide');*/
}
function ResetTimers1(){
    clearTimeout(warningTimer);
    clearTimeout(timeoutTimer);
    StartTimers();
    $("#logout_popup").modal('hide');
}

// Show idle timeout warning dialog.
function IdleWarning() {
    $("#logout_popup").modal("show");
        
}

// Logout the user.
function IdleTimeout() {
    window.location = logoutUrl;
}