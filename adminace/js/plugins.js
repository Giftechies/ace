$(function () {
    $('#example').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "drawCallback": function( settings ) {

            // popover
            $('[data-toggle="popover"]').popover({
                placement : 'top',
                trigger : 'hover'
            }); 
            
            //delete confirm popup
            $('.delete').click(function(){
            var id= $(this).attr('id');
            var href= $(this).attr('data-href');
            $('.del-confirm').attr('href',href+id);
            });
            //delete confirm popup

        },
      responsive: true,
      columnDefs: [
          { responsivePriority: 1, targets: 0 },
          { responsivePriority: 2, targets: -1 }
      ]
    });
    
    // datepicker for slider start date
    $('#s_date').datepicker({
      autoclose: true
    });
    
    // datepicker for slider end date
	 $('#e_date').datepicker({
      autoclose: true
    });
    
    // html Editor
    CKEDITOR.replace('editor1');
    
    // Custom select box select 2
    $(".select2").select2();
    
    
    
});