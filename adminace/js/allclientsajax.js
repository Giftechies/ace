// initialize datatable on adalbums page
   $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "drawCallback": function( settings ) {

            // popover
            $('[data-toggle="popover"]').popover({
                placement : 'top',
                trigger : 'hover'
            }); 
            
            //delete confirm popup
            $('.delete').click(function(){
            var id= $(this).attr('id');
            var href= $(this).attr('data-href');
            $('.del-confirm').attr('href',href+id);
            });
            //delete confirm popup
            
            // edit album function in same field 
            $('.edit-btn').click(function(){
                var id= $(this).attr('id');
                $.ajax({
                    url: "views/editclientajax.php",
                    type: "POST",
                    data: {edit : id},
                    beforeSend: function(){
                         $('.cssload-whirlpool1').show();
                         $('.addform').fadeTo(0,0.1);
                     },
                    success: function(data) {
                     $('.form-box').html(data);
                     $('.cssload-whirlpool1').delay(2000).fadeOut();
                     $('.addform').delay(2000).fadeTo(0, 1);
                     disablesubmit();
                    window.scrollTo(0,0);
                    }
                });
    });
    
    //call to localdate function

        },
    });
    
    // form submition for adding albums & images
    $("#clients_form").submit(function(event){
        event.preventDefault(); //prevent default action
        var post_url = $(this).attr("action"); //get form action url
        var request_method = $(this).attr("method"); //get form GET/POST method
        var formData = new FormData($(this)[0]); //Encode form elements for submission
        $.ajax({
            url : post_url,
            type: request_method,
            data : formData,
            processData: false,
            contentType: false,
            beforeSend: function(){
                 $('.cssload-whirlpool').show();
                 $('.album-box').fadeTo(0,0.1);
                 $('.cssload-whirlpool1').show();
                 $('.addform').fadeTo(0,0.1);
                $('#title').val('');
                $('#coverimg').val('');
                $('#coverinput').val('');
                $('.file-preview-thumbnails').html('');
                $('.file-drop-zone').append('<div class="file-drop-zone-title">Drag &amp; drop files here …</div>');
                $('.kv-fileinput-caption').html('');
                $('.file').val('');
                $('.image-preview').popover('hide');
                $('.image-preview').attr('data-content','');
                $('.image-preview-clear').hide();
             }
        }).done(function(response){ //
            $("#result").html(response);
            $.ajax({
                url: "views/allclientsajax.php",
                type: "POST",
                data: 'data',
                success: function(data) {
                 $('.albumdata').html(data);
                 $('.cssload-whirlpool').delay(2000).fadeOut();
                 $('.album-box').delay(2000).fadeTo(0, 1);
                    $('.cssload-whirlpool1').delay(2000).fadeOut();
                    $('.addform').delay(2000).fadeTo(0, 1);
                 $(".image-preview-input-title").text("Browse");
                 disablesubmit();
                }
            });
        
        });
    });
    // form submition for adding albums & images
    
    // popover
    $('[data-toggle="popover"]').popover({
        placement : 'top',
        trigger : 'hover'
    }); 
    // popover
    
    //deleteimagefilter('#filter'); function
    $('.delete').click(function(){
    var id= $(this).attr('id');
    var href= $(this).attr('data-href');
    $('.del-confirm').attr('href',href+id);
    });
    
    // cancle update
    $('.cancle').click(function(){
        //get all albums 
        $.ajax({
                url: "views/allclientsajax.php",
                type: "POST",
                data: 'data',
                success: function(data) {
                $("#result").html('');
                 $('.albumdata').html(data);
                }
            });
        //get all albums
    });
    // cancle update
    
    // edit album function in same field 
    $('.edit-btn').click(function(){
        var id= $(this).attr('id');
        $.ajax({
            url: "views/editclientajax.php",
            type: "POST",
            data: {edit : id},
            beforeSend: function(){
                 $('.cssload-whirlpool1').show();
                 $('.addform').fadeTo(0,0.1);
             },
            success: function(data) {
             $('.form-box').html(data);
             $('.cssload-whirlpool1').delay(2000).fadeOut();
             $('.addform').delay(2000).fadeTo(0, 1);
             disablesubmit();
            window.scrollTo(0,0);
            }
        });
    });
    
    //call to localdate function
   //getting local datetime
    setInterval(localdate(), 1000);
    
    
    
    
    
   