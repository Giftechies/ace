

// initialize datatable on adalbums page
   $('#example4').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      columnDefs: [
        { responsivePriority: 1, targets: 0 },
        { responsivePriority: 2, targets: -1 }
     ],
      "drawCallback": function( settings ) {

            // popover
            $('[data-toggle="popover"]').popover({
                placement : 'top',
                trigger : 'hover'
            }); 
            
            //delete confirm popup
            $('.delete').click(function(){
            var id= $(this).attr('id');
            var href= $(this).attr('data-href');
            $('.del-confirm').attr('href',href+id);
            });
            //delete confirm popup

        },
    });
    
    //delete video function
    $('.delete').click(function(){
    var id= $(this).attr('id');
    var href= $(this).attr('data-href');
    $('.del-confirm').attr('href',href+id);
    });
     //delete video function
    
   // popover
    $('[data-toggle="popover"]').popover({
        placement : 'top',
        trigger : 'hover'
    });
     // popover
     
    // update video Form Submi
    function editvideo(id,href,resultfield){
        var id1= id;
        var href1=href;
         $('.cssload-whirlpool1').show();
         $('.addform').fadeTo(0,0.1);
        $.ajax({
            url: href1,
            type: "GET",
            data: {edit : id1},
            success: function(data) {
             $(resultfield).html(data);
             disablesubmit();
             $('.cssload-whirlpool1').delay(2000).fadeOut();
             $('.addform').delay(2000).fadeTo(0, 1);
            window.scrollTo(0,0);
            }
        });
    }
    