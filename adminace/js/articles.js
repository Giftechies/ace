$.ajax({
        url: "views/allarticlesajax.php",
        type: "POST",
        data: 'data',
        success: function(data) {
         $('.articlesdata').html(data);
        $('.cssload-whirlpool').delay(2000).fadeOut();
        $('.album-box').delay(2000).fadeTo(0, 1);
        }
    });
            
// tinymce plugin
tinymce.init({
        selector: ".t",
        theme: 'modern',
        mode : "textareas",
		plugins: [
					"advlist save emoticons autolink lists link image charmap print preview anchor",
					"searchreplace visualblocks code fullscreen tabfocus wordcount spellchecker colorpicker",
					"insertdatetime media table contextmenu textpattern paste imagetools colorpicker directionality hr textcolor toc legacyoutput"
				],
		toolbar: "leaui_formula, | styleselect, fontselect, fontsizeselect, | undo, redo, | alignleft, aligncenter, alignright, alignjustify, | bullist, numlist, indent, outdent, | print, preview, fullpage,media | forecolor, backcolor, emoticons, bold, italic, underline, strikethrough, link",
        force_br_newlines : true,
        force_p_newlines : false,
        forced_root_block : '',
       
    
});
 
// tinymce plugin

  setInterval(localdate(), 1000);

// form submition for adding about details
    $("#articles_form").submit(function(event){
        event.preventDefault(); //prevent default action  
            $("#result").html('');
        tinyMCE.triggerSave();
        var post_url = $(this).attr("action"); //get form action url
        var request_method = $(this).attr("method"); //get form GET/POST method
        var formData = new FormData($(this)[0]); //Encode form elements for submission
        $.ajax({
            url : post_url,
            type: request_method,
            data : formData,
            processData: false,
            contentType: false,
            beforeSend: function(){
                 $('.cssload-whirlpool').show();
                 $('.album-box').fadeTo(0,0.1);
             }
        }).done(function(response){ //
            $("#result").html(response);
            $('#mtitle').val('');
            $('#mdesc').val('');
            $('#title').val('');
            $('#mtitle').val('');
            $('.t').val('');
            $.ajax({
                url: "views/allarticlesajax.php",
                type: "POST",
                data: 'data',
                success: function(data) {
                 $('.articlesdata').html(data);
                $('.cssload-whirlpool').delay(2000).fadeOut();
                $('.album-box').delay(2000).fadeTo(0, 1);
                }
            });
            $.ajax({
                    url: "views/editarticleajax.php",
                    type: "POST",
                    data: 'data',
                    success: function(data) {
                     $('.articlesform').html(data);
                    }
                });
        
        });
    });
    // form submition for adding about details
    function editarticle(id,href){
        $.ajax({
                url: href,
                type: "POST",
                data: {edit:id},
                beforeSend: function(){
                     $('.cssload-whirlpool1').show();
                     $('.articlesform').fadeTo(0,0.1);
                 },
                success: function(data) {
                 $('.articlesform').html(data);
                 $('.cssload-whirlpool1').delay(2000).fadeOut();
                 $('.articlesform').delay(2000).fadeTo(0, 1);
                 // html Editor
                }
            });
    }
    $("#add-article").click(function(){
         var link = $(this);
         if ($('.custom-form').is(':visible')) {
             link.text('Add Article');                
        } else {
             link.text('close');                
        }        
          $(".custom-form").slideToggle();
     });
     
/*function imagetype(id) {
    var filename = $('#'+id).val();

        // Use a regular expression to trim everything before final dot
        var ext = filename.replace(/^.*\./, '');

        if (ext == filename) {
            ext = '';
        } else {
            ext = ext.toLowerCase();
        }
    switch (ext) {
        case 'jpg':
        case 'jpeg':
        case 'png':
        case 'gif':
            break;
        default:
        $('.filealert').show();
            this.value = '';
    }
}*/