<?php session_start();
require_once('config/config.php');
if(isset($_SESSION['user'])){
    header('location: dashboard.php');
}else{?>
<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>ACE Channel Login</title>
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="login/css/style.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>

</head>

<body>
  
<div class="wrapper">
  <?php if(isset($_POST['forgot'])){
      function izrand($length = 6) {
        $random_string="";
        while(strlen($random_string)<$length && $length > 0) {
                $randnum = mt_rand(0,61);
                $random_string .= ($randnum < 10) ?
                        chr($randnum+48) : ($randnum < 36 ? 
                                chr($randnum+55) : $randnum+61);
         }
        return $random_string;
      }
      $username=$_POST['username'];
      $q="select * from user where username='$username'";
        $run=mysqli_query($conn,$q);
        if(mysqli_num_rows($run)>0){
          while($row=mysqli_fetch_array($run)){
            $length=6;
            $token=izrand($length);
            $q1="update user set token='$token' where username='$username'";
            $run1=mysqli_query($conn,$q1);
            $link="<a href='https://ace-giftechies818.c9users.io/adminace/index.php?token=".$token."'>Reset Password</a>";
            $email=$row['useremail'];
            $username=$row['username'];
							    require 'PHPMailer/PHPMailerAutoload.php';
									//Create a new PHPMailer instance
									$mail = new PHPMailer;
									//Set who the message is to be sent from
									$mail->setFrom('info@acechannel.com', 'Bookings For Hotel');
									//Set who the message is to be sent to
									$mail->addAddress($email, 'Password Reset Link');
									//Set the subject line
									$mail->Subject = 'Password Reset Link';
									//Read an HTML message body from an external file, convert referenced images to embedded,
									$mail->Body = "Username :".ucwords($username)."<br />\n\n"
												  . "Password Reset Link :\n\n ".$link."<br />\n"
												  . "Expiry :\n\n Half an Hour<br />\n"
												  . "";
									$mail->AltBody = 'Mail Regarding Bookings For Hotel';
									
									//send the message, check for errors
									if (!$mail->send()) {
										echo "<div class='alert alert-danger'> Mailer Error: " . $mail->ErrorInfo. "</div>";
									} else {
										echo "<div class='alert alert-success'> Check Your Email </div>";
									}
							}
							
          }else{
          echo "<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
							<span class=\"bold\">Error: </span>  Username not Exist. </div>";
        }
    }
  ?>
  <?php if(isset($_POST['login'])){
      $username=$_POST['username'];
      $password=md5($_POST['password']);
      $q="select * from user where username='$username' and password_hash='$password'";
        $run=mysqli_query($conn,$q);
        if(mysqli_num_rows($run)>0){
          $_SESSION['user']=$username;
          $_SESSION['pass']=$password;
          $_SESSION['last_login_timestamp'] = time();
          header('location: dashboard.php');
        }else{
          echo "<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
							<span class=\"bold\">Error: </span>  Wrong Username or Passworrd. </div>";
        }
    }
  ?>
  <form class="login" action="#" method="post" enctype="multipart/form-data">
    <p class="title">Log in</p>
    <input type="text" placeholder="Username" name="username" autofocus/>
    <i class="fa fa-user"></i>
    <input type="password" name="password" placeholder="Password" />
    <i class="fa fa-key"></i>
    <a href="#myModal" data-toggle="modal" data-target="#myModal">Forgot your password?</a>
    <button type="submit" name="login">
      <i class="spinner"></i>
      <span class="state">Log in</span>
    </button>
  </form>
  <footer><a target="blank" href="https://ace-giftechies818.c9users.io/">acechannel.com</a></footer>
  </p>
  
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Forgot Password</h4>
        </div>
        <div class="modal-body">
          <form class="loginnew" action="#" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label for="username">Fill Username:</label>
                <input type="text" class="form-control" name="username" id="user">
              </div>
              <button type="submit" class="btn btn-primary" name="forgot">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/js/bootstrap.min.js'></script>
</body>
</html>
<?php } ?>
