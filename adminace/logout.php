<?php ob_start(); session_start();
if(!isset($_SESSION['user'])){
    header('location: index.php');
}else{?>
<?php include_once('views/header.php');?>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        ACE Channel
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Log Out</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        
        <?php if($_SESSION['user']){
              unset($_SESSION['user']);
              echo '<div id="result col-xs-12">
                      <div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
						            <span class=\"bold\">Success: </span>  You are Successfully Logout. 
						          </div>
						        </div>';
              header('location: index.php');
          }
          ?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include_once('views/footer.php');?>
<?php } ?>