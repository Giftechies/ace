<?php function paginate($item_per_page, $current_page, $total_records, $total_pages, $page_url)
{
    $pagination = '';
    if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
        $pagination .= '<ul class="pagination">';
        
        $right_links    = $current_page + 3; 
        $previous       = $current_page - 3; //previous link 
        $next           = $current_page + 1; //next link
        $first_link     = true; //boolean var to decide our first link
        
        if($current_page > 1){
            $previous_link = ($previous==0)?1:$previous;
            $pagination .= '<li class="first"><a href="'.$page_url.'?page=1" title="First">&laquo;</a></li>'; //first link
            $pagination .= '<li><a href="'.$page_url.'?page='.$previous_link.'" title="Previous">&lt;</a></li>'; //previous link
                for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
                    if($i > 0){
                        $pagination .= '<li><a href="'.$page_url.'?page='.$i.'">'.$i.'</a></li>';
                    }
                }   
            $first_link = false; //set first link to false
        }
        
        if($first_link){ //if current active page is first link
            $pagination .= '<li class="first active"><a>'.$current_page.'</a></li>';
        }elseif($current_page == $total_pages){ //if it's the last active link
            $pagination .= '<li class="last active"><a>'.$current_page.'</a></li>';
        }else{ //regular current link
            $pagination .= '<li class="active"><a>'.$current_page.'</a></li>';
        }
                
        for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
            if($i<=$total_pages){
                $pagination .= '<li><a href="'.$page_url.'?page='.$i.'">'.$i.'</a></li>';
            }
        }
        if($current_page < $total_pages){ 
                $next_link = ($i > $total_pages)? $total_pages : $i;
                $pagination .= '<li><a href="'.$page_url.'?page='.$next_link.'" >&gt;</a></li>'; //next link
                $pagination .= '<li class="last"><a href="'.$page_url.'?page='.$total_pages.'" title="Last">&raquo;</a></li>'; //last link
        }
        
        $pagination .= '</ul>'; 
    }
    return $pagination; //return pagination links
}
	
function createThumbnail($filename,$originalpath,$thumbpath,$width=236,$height=160) {
			$im="";
			$oi_path="$originalpath";
			$path_thumbs="$thumbpath";
					if(preg_match('/[.](jpg)$/', $filename)) {
						$im = imagecreatefromjpeg($oi_path . $filename);
					} else if (preg_match('/[.](gif)$/', $filename)) {
						$im = imagecreatefromgif($oi_path . $filename);
					} else if (preg_match('/[.](png)$/', $filename)) {
						$im = imagecreatefrompng($oi_path . $filename);
					}
					 $ox="";
					 $oy="";
					$ox = imagesx($im);
					$oy = imagesy($im);
					 
					$nx = 280;
					$ny = floor($oy * ($nx / $ox));
					 
					$nm = imagecreatetruecolor($nx, $ny);
					 
					imagecopyresized($nm, $im, 0,0,0,0,$nx,$ny,$ox,$oy);
					 
					if(!file_exists($path_thumbs)) {
					  if(!mkdir($path_thumbs)) {
						   die("There was a problem. Please try again!");
					  } 
					}
				 
					imagejpeg($nm, $path_thumbs . $filename);
					
					$dst_x = 0;   // X-coordinate of destination point
					$dst_y = 0;   // Y-coordinate of destination point
					$src_x = 0; // Crop Start X position in original image
					$src_y = 0; // Crop Srart Y position in original image
					$dst_w = $width; // Thumb width
					$dst_h = $height; // Thumb height
					$src_w = 220; // Crop end X position in original image
					$src_h = 150; // Crop end Y position in original image
					
					$dst_image = imagecreatetruecolor($dst_w, $dst_h);
					// Get original image
					$src_image = imagecreatefromjpeg($thumbpath.$filename);
					// Cropping
					imagecopyresampled($dst_image, $src_image, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
					// Saving
					imagejpeg($dst_image, $thumbpath.$filename);
					
					$tn = '<img src="' . $path_thumbs .$filename. '" alt="image" />';
					return $tn;
	}
?>