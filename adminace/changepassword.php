<?php ob_start(); session_start();
if(!isset($_SESSION['user'])){
    header('location: index.php');
}else{?>
<?php include_once('views/header.php');?>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        ACE Channel
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Change Password</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php if(isset($_POST['update'])){
  			$password=$_POST['pass'];
  			$new_pass1=$_POST['new_pass1'];
  			$new_pass2=$_POST['new_pass2'];
  			if((($password=="") or ($new_pass1=="") or ($new_pass2==""))){
  				echo"  <div class=\"col-sm-12\">   
  				          <div class=\"col-sm-12 alert alert-danger alert-dismissible\">
  										<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
  										Please Fill All Fields.
  								  </div>
  							</div>";
  				}
  			else if(($new_pass1==$new_pass2)){
  			  $pass_hash=md5($new_pass1);
  				$query="select * from user where password_hash='".md5($password)."'";
  					$run=mysqli_query($conn,$query);
  						if(mysqli_num_rows($run) > 0){
  					while($row=mysqli_fetch_array($run)){
  							$query1="update user set password='".mysqli_real_escape_string($conn,$new_pass1)."', password_hash='".mysqli_real_escape_string($conn,$pass_hash)."'  where password_hash='".mysqli_real_escape_string($conn,md5($password))."'";
  							$run1=mysqli_query($conn,$query1);
  							if($run1){
  								echo "<div class=\"col-sm-12\">
  								        <div class=\"col-sm-12 alert alert-success alert-dismissible\">
        										<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
        										Password Updated Successfully.
        								  </div>
      								  </div>";
  								if($_SESSION['user']){
  									unset($_SESSION['user']);
  									header('location: index.php');
  									}
  							}
  						}
  					}
  						else{
  						echo "<div class=\"col-sm-12\">
  						        <div class=\"col-sm-12 alert alert-danger alert-dismissible\">
    										<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
    										Please Check Password.
    								  </div>
    								</div>";
  						}
  					
  				}
  			else {
  				echo "<div class=\"col-sm-12\">
				            <div class=\"col-sm-12 alert alert-danger alert-dismissible\">
  										<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
  										New Password Not Matched.
  								  </div>
  							</div>";
  				}
  		}
  		?>
      <div class="col-md-6">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Change Password</h3>
          </div>
          <form action="#" method="post" enctype="multipart/form-data">
            <div class="box-body">
    					<div class="form-group">
    					<label for="pass">Password</label>
    					<input type="password" class="form-control" name="pass" placeholder="Old Password" id="pass">
    					</div>
    					<div class="form-group">
    					<label for="new_pass">New Password</label>
    					<input type="password" class="form-control" name="new_pass1" placeholder="New Password" id="new_pass">
    					</div>
    					<div class="form-group">
    					<label for="new_pass2">Confirm Password</label>
    					<input type="password" class="form-control" name="new_pass2" placeholder="Confirm New Password" id="new_pass2">
    					</div>
            </div>
    				<div class="box-footer">
      					<div class="form-group form-horizontal text-left">
      					  <input type="submit" class="btn btn-danger" name="update" value="Update" id="new_pass2">
      					</div>
    				</div>
  			</form>
  		</div>
  		</div>
  		<div class="clearfix"></div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include_once('views/footer.php');?>
<?php } ?>