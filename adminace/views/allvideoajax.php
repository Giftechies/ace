<?php header('Access-Control-Allow-Origin: *'); require_once('../config/config.php'); ?>
<table id="example4" class="table table-striped table-bordered dt-responsive nowrap">
    <thead>
    <tr>
      <th>Sr.</th>
      <th>Title</th>
      <th class="text-center">Thumbnail</th>
      <th>URL</th>
      <th>Created</th>
      <th class="text-center">Actions</th>
    </tr>
    </thead>
    <tbody>
      <?php 
        //insert query//
        $r="select * from videos order by 1 desc";
        $sr=1;
        $result=mysqli_query($conn,$r) or die (mysql_error());
        while($row=mysqli_fetch_array($result,MYSQL_ASSOC)){
            $id=$row['id'];
        	$title=$row['title'];
        	$URL=$row['url'];
        	$created=$row['created'];
        	$lastupdate=$row['lastupdate'];
        ?>
        <tr>
          <td><?php echo $sr;?></td>
          <td>
                  <?php echo strlen($title) > 20 ? ucwords(substr($title,0,20))."..." : ucwords($title);?>
          </td>
          <td class="text-center"><img src="<?php getYoutubeImage($URL)?>" height="50px" width="50px" /></td>
          <td> <?php echo strlen($URL) > 20 ? substr($URL,0,20)."..." : $URL; ?> </td>
          <td>
              <span class="pull-left">
                  <?php echo $created;?>
              </span>
              <?php if($lastupdate != ''){?>
              <span class="pull-right" href="#" class="pull-right" data-toggle="popover" title="Last Update" data-content="<?php echo $lastupdate;?>">
                  <i class="fa fa-pencil"></i>
              </span>
              <?php } ?>
          </td>
          <td class="text-center">
    			<button id="<?php echo $id; ?>" title="edit" onclick="editvideo(id,'views/editvideo.php','.addform')" class="btn btn-success">
      		  <i class="fa fa-pencil-square-o"></i>
      		</button>
      		<button
      		<?php echo 'title="Delete" id='.$id.' class="btn btn-danger delete" data-href="addvideo.php?del=" data-toggle="modal" data-target="#myModal"><i class="fa fa-trash-o"></i>';
      		        ?> 
      		</button>
    			</td>
        </tr>
      <?php $sr++;}?>

    </tbody>
</table>
<?php
      function getYoutubeImage($e){
      //GET THE URL
      $url = $e;

      $queryString = parse_url($url, PHP_URL_QUERY);

      parse_str($queryString, $params);

      $v = $params['v'];  
      //DISPLAY THE IMAGE
      if(strlen($v)>0){
          echo "http://img.youtube.com/vi/$v/0.jpg";
      }
  }
?>
<script src="js/allvideoajax.js"></script>