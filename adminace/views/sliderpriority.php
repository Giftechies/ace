<?php
require_once('../config/config.php');
    if(isset($_POST)) {
    $id_ary = explode(",",$_POST["row_order"]);
      for($i=0;$i<count($id_ary);$i++) {
        $q="UPDATE slider SET priority='" . $i . "' WHERE id=". $id_ary[$i];
        $run=mysqli_query($conn,$q);
        if($run){
            $msg="<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
							<span class=\"bold\">Success: </span>  Successfully Updated. </div>";
        }
      }
    }
  ?>
  <div class="priority-data"><?php echo $msg;?></div>
  <form id="priorityupdate" action="views/sliderpriority.php" method="POST" />
    <input type="hidden" name="row_order" id="row_order" /> 
    <ul id="sortable-row">
      <?php $q1="select * from slider where status=1 order by priority";
            $run=mysqli_query($conn,$q1);
            while($row=mysqli_fetch_array($run)){
      ?>
      <li id="<?php echo $row['id']; ?>"><?php echo $row['image_title']; ?></li>
      <?php } ?>
    
     
    </ul>
    <input type="submit" class="btnSave" name="submit" value="Save Order" onClick="saveOrder();" />
  </form> 
  <script>
        $( "#sortable-row" ).sortable({
    	placeholder: "ui-state-highlight"
    	});
      
      function saveOrder() {
    	var title = new Array();
    	$('ul#sortable-row li').each(function() {
    	title.push($(this).attr("id"));
    	});
    	document.getElementById("row_order").value = title;
    	$("#priorityupdate").submit();
      }
      
      $("#priorityupdate").submit(function(event){
            event.preventDefault(); //prevent default action  
            var data = $('#row_order').val();
            $.ajax({
                url : 'views/sliderpriority.php',
                type: 'POST',
                data : {row_order:data}
            }).done(function(response){ 
                $(".priority-box").html(response);
            });
        });
      
      
</script>