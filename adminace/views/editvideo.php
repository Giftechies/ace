<?php
require_once('../config/config.php');
if(isset($_GET['edit'])){ 
    $id=$_GET['edit'];
    $p="select * from videos where id='$id'";
    $run=mysqli_query($conn,$p);
    while($row=mysqli_fetch_array($run)){
        $id=$row['id'];
        $title=$row['title'];
        $url=$row['url'];
    }
?>
            <div class="box-header with-border">
              <h3 class="box-title">Update Video</h3>
              <button id="cancle" title="Cancle" name="cancle" onclick="cancle('views/editvideo.php','.addform')" class="btn btn-warning pull-right">
                 Cancle
              </button>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="update" action="ajax/videosajax.php" method="POST" enctype="multipart/form-data">
              <div class="box-body">
  					<div Class="form-group col-sm-4 title-height field">
  						<label for="title">Video Title</label>
  						<input type="hidden" name="id" value="<?php echo $id;?>" id="">
  						<input type="hidden" name="form" value="update" class="form-control" required>
  						<input type="hidden" name="date" value="" id="localdate">
  						<input type="text" name="title" id="title" value="<?php echo $title;?>"  class="form-control"  required>
  					</div>
  					<div Class="form-group col-sm-4 field">
  						<label for="url">Video URL</label>
  						<input type="text" name="url" id="url" value="<?php echo $url;?>" pattern="https?://.+" class="form-control" required>
  					</div>
  					<div Class="form-group col-sm-4 btn-height">
  					  <input type="submit" name="submit" value="Update" id="submit" class="btn btn-primary">
  					</div>
              </div>
              <!-- /.box-body -->
            </form>
<script>
    // form submition for adding Videos
    $("#update").submit(function(event){
        event.preventDefault(); //prevent default action 
        var post_url = $(this).attr("action"); //get form action url
        var request_method = $(this).attr("method"); //get form GET/POST method
        var formData = new FormData($(this)[0]); //Encode form elements for submission
        $.ajax({
            url : post_url,
            type: request_method,
            data : formData,
            processData: false,
            contentType: false,
            beforeSend: function(){
                 $('.cssload-whirlpool1').show();
                 $('.addform').fadeTo(0,0.1);
                 $('.cssload-whirlpool').show();
                 $('.video-box').fadeTo(0,0.1);
             }
        }).done(function(response){ //
            $("#result").html(response);
            $('#title').val('');
            $('#url').val('');
            $.ajax({
                url: "views/allvideoajax.php",
                type: "POST",
                data: 'data',
                success: function(data) {
                 $('.videodata').html(data);
                }
            });
            $.ajax({
                url: "views/editvideo.php",
                type: "POST",
                data: 'data',
                success: function(data) {
                 $('.addform').html(data);
                 $('.cssload-whirlpool').delay(2000).fadeOut();
                 $('.video-box').delay(2000).fadeTo(0, 1);
                 $('.cssload-whirlpool1').delay(2000).fadeOut();
                 $('.addform').delay(2000).fadeTo(0, 1);
                    disablesubmit();
                }
            });
        
        });
    });
    // form submition for adding Videos
    $('.field input').on('keyup keydown blur change',function() {
        
        $('.field input').each(function() {
            var empty="";
            if ($(this).val() == '') {
                empty = true;
            }
             if (empty) {
            $('#submit').attr('disabled', true);
            } else {
                $('#submit').attr('disabled', false);
            }
        });

       
    });
    //getting local datetime
    setInterval(localdate(), 1000);
    equalheight(".title-height",".btn-height");
</script>
<?php }else{ ?>
            <div class="box-header with-border">
              <h3 class="box-title">Add Video</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="video_form" action="ajax/videosajax.php" method="POST" enctype="multipart/form-data">
              <div class="box-body">
  					<div Class="form-group col-sm-4 title-height field">
  						<label for="title">Video Title</label>
  						<input type="hidden" name="form" value="add" class="form-control" required>
  						<input type="hidden" name="date" value="" id="localdate">
  						<input type="text" name="title" id="title" value=""  class="form-control"  required>
  					</div>
  					<div Class="form-group col-sm-4 field">
  						<label for="url">Video URL</label>
  						<input type="text" name="url" id="url" value="" pattern="https?://.+" class="form-control" required>
  					</div>
  					<div Class="form-group col-sm-4 btn-height">
  					  <input type="submit" name="submit" value="Submit" id="submit" class="btn btn-primary">
  					</div>
              </div>
              <!-- /.box-body -->
            </form>
<script>
// form submition for adding Videos
    $("#video_form").submit(function(event){
        event.preventDefault(); //prevent default action 
        var post_url = $(this).attr("action"); //get form action url
        var request_method = $(this).attr("method"); //get form GET/POST method
        var formData = new FormData($(this)[0]); //Encode form elements for submission
        $.ajax({
            url : post_url,
            type: request_method,
            data : formData,
            processData: false,
            contentType: false,
            beforeSend: function(){
                 $('.cssload-whirlpool1').show();
                 $('.addform').fadeTo(0,0.1);
                 $('.cssload-whirlpool').show();
                 $('.video-box').fadeTo(0,0.1);
             }
        }).done(function(response){ //
            $("#result").html(response);
            $('#title').val('');
            $('#url').val('');
            $.ajax({
                url: "views/allvideoajax.php",
                type: "POST",
                data: 'data',
                success: function(data) {
                 $('.videodata').html(data);
                }
            });
            $.ajax({
                url: "views/editvideo.php",
                type: "POST",
                data: 'data',
                success: function(data) {
                 $('.addform').html(data);
                 $('.cssload-whirlpool').delay(2000).fadeOut();
                 $('.video-box').delay(2000).fadeTo(0, 1);
                 $('.cssload-whirlpool1').delay(2000).fadeOut();
                 $('.addform').delay(2000).fadeTo(0, 1);
                    disablesubmit();
                }
            });
        
        });
    });
    // form submition for adding Videos
    
    //getting local datetime
    setInterval(localdate(), 1000);
     disablesubmit();
     equalheight(".title-height",".btn-height");
   $('.field input').on('keyup keydown blur change',function() {
        
        $('.field input').each(function() {
            var empty="";
            if ($(this).val() == '') {
                empty = true;
            }
             if (empty) {
            $('#submit').attr('disabled', true);
            } else {
                $('#submit').attr('disabled', false);
            }
        });

       
    });
</script>
<?php }?>