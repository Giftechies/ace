<?php require_once('../config/config.php');?>
	     <table id="example" class="table table-striped table-bordered dt-responsive nowrap">
            <thead>
            <tr>
              <th>Sr.</th>
              <th>Image</th>
              <th>Image Title</th>
              <th>Image ALT</th>
              <th>Detination URL</th>
    				  <th>Status</th>
    				  <th class="text-center">Actions</th>
            </tr>
            </thead>
            <tbody>
           <?php 
                //insert query//
                $r="select * from slider order by 1 desc";
                $sr=1;
                $result=mysqli_query($conn,$r) or die (mysql_error());
                while($row=mysqli_fetch_array($result,MYSQL_ASSOC)){
                	$id=$row['id'];
                	$image_name=$row['image_name'];
                	$image_alt=$row['image_alt'];
                	$image_title=$row['image_title'];
                	$status=$row['status'];
                	$destination_url=$row['destination_url'];
                
                ?>
            <tr>
              <td><?php echo $sr;?></td>
              <td><img src="../images/slides/<?php echo $image_name;?>" height="50px" width="50px" /></td>
              <td><?php echo strlen(ucwords($image_title)) > 20 ? substr(ucwords($image_title),0,20)."..." : ucwords($image_title); ?></td>
              <td><?php echo $image_alt;?></td>
    				  
    				  <td><?php echo strlen($destination_url) > 20 ? substr($destination_url,0,20)."..." : $destination_url; ?></td>
    				  <td><?php echo ($status==0)?'<button id="'.$id.'" class="btn btn-success" onclick="doactive(id)">Active</button>':'<button id="'.$id.'" class="btn btn-warning" onclick="doinactive(id)">Inactive</button>';?></td>
              <td>
    						<button id="<?php echo $id; ?>" title="Edit" class="btn btn-success" onclick="editslider(id,'views/editsliderajax.php?id=')"><i class="fa fa-pencil-square-o"></i></button>
    						<button title="Delete" id="<?php echo $id; ?>" gettitle="<?php echo ucwords($image_title);?>" class="btn btn-danger delete" data-href="slider.php?del=" data-toggle="modal" data-target="#myModal"><i class="fa fa-times"></i></button></td>
              </tr>
              <?php $sr++;	}?>

            </tbody>
       </table>

    <!-- /.box-body -->
<script>
  
  //delete confirm popup
      $('.delete').click(function(){
      var id= $(this).attr('id');
      var href= $(this).attr('data-href');
      var title= $(this).attr('gettitle');
      $('.del-confirm').attr('href',href+id);
            $('.gettitle').html(title);
      });
  //delete confirm popup
    
  // initialize datatable on about page
   $('#example').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "drawCallback": function( settings ) {
            //delete confirm popup
            $('.delete').click(function(){
            var id= $(this).attr('id');
            var title= $(this).attr('gettitle');
            var href= $(this).attr('data-href');
            $('.del-confirm').attr('href',href+id);
            $('.gettitle').html(title);
            });
            //delete confirm popup

        },
    });
     function doactive(id){
         $.ajax({
                url: "views/changestatus.php",
                type: "POST",
                data: {active:id},
                success: function(data) {
                        $.ajax({
                        url: "views/allslidesajax.php",
                        type: "POST",
                        data: 'data',
                        beforeSend: function(){
                             $('.cssload-whirlpool').show();
                             $('.album-box').fadeTo(0,0.1);
                         },
                        success: function(data) {
                         $('.sliderdata').html(data);
                        $('.cssload-whirlpool').delay(2000).fadeOut();
                        $('.album-box').delay(2000).fadeTo(0, 1);
                        }
                    });
                    $.ajax({
                        url: 'views/sliderpriority.php',
                        type: "POST",
                        data: 'data',
                        success: function(data) {
                         $('.priority-box').html(data);
                         }
                    });
                }
            });
     }
     function doinactive(id){
         $.ajax({
                url: "views/changestatus.php",
                type: "POST",
                data: {inactive:id},
                success: function(data) {
                        $.ajax({
                        url: "views/allslidesajax.php",
                        type: "POST",
                        data: 'data',
                        beforeSend: function(){
                             $('.cssload-whirlpool').show();
                             $('.album-box').fadeTo(0,0.1);
                         },
                        success: function(data) {
                         $('.sliderdata').html(data);
                        $('.cssload-whirlpool').delay(2000).fadeOut();
                        $('.album-box').delay(2000).fadeTo(0, 1);
                        }
                    });
                    $.ajax({
                        url: 'views/sliderpriority.php',
                        type: "POST",
                        data: 'data',
                        success: function(data) {
                         $('.priority-box').html(data);
                         }
                    });
                }
            });
     }
</script>