<?php
require_once('../config/config.php');
if(isset($_POST['edit'])){
    $id=$_POST['edit'];
    $p="select * from articles where id=$id";
    $run=mysqli_query($conn,$p);
    while($row=mysqli_fetch_array($run)){
        $id=$row['id'];
        $title=$row['title'];
        $url=$row['videourl'];
        $image=$row['image'];
        $content=$row['content'];
        $mtitle=$row['mtitle'];
        $mdesc=$row['mdesc'];
    }
?>
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Articles</h3>
                <button class="btn btn-warning pull-right" onclick="cancle('views/editarticleajax.php','.articlesform')">Cancle</button>
                
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form id="update" action="ajax/articlesajax.php" method="POST" enctype="multipart/form-data">
                <div class="box-body">
                      
                    <div class="col-sm-12">
      					<div Class="form-group field">
      						<label for="title">Title</label>
      						<input type="text" name="title" id="title" class="form-control" value="<?php echo $title;?>" title="Article Title">
      						<input type="hidden" name="form" value="update" class="form-control" required>
      						<input type="hidden" name="id" value="<?php echo $id;?>" class="form-control" required>
      						<input type="hidden" name="oldimg" value="<?php echo $image;?>" class="form-control" required>
    						<input type="hidden" name="date" value="" id="localdate">
      					</div>
      					<div Class="form-group">
          					<label for="desc">Article Image</label>
          					<div class="input-group image-preview">
                              <input id="coverinput" type="text" class="form-control image-preview-filename" disabled="disabled" title="Please Browse an Image"> <!-- don't give a name === doesn't send on POST/GET -->
                              <span class="input-group-btn">
                                  <!-- image-preview-clear button -->
                                  <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                      <span class="glyphicon glyphicon-remove"></span> Clear
                                  </button>
                                  <!-- image-preview-input -->
                                  <div class="btn btn-default field image-preview-input">
                                      <span class="glyphicon glyphicon-folder-open"></span>
                                      <span class="image-preview-input-title">Browse</span>
                                      <input id="coverimg" type="file" accept="image/*" name="image"/> <!-- rename it -->
                                  </div>
                              </span>
                          </div>
                        </div>
      					<div Class="form-group field">
      						<label>Article Content</label>
  			                <textarea name="content" class="t" rows="10" cols="80" title="Article Content"><?php echo $content;?></textarea>
      					</div>
      					<div Class="form-group field">
      						<label for="mtitle">Meta Title</label>
      						<input type="text" name="mtitle" id="mtitle" class="form-control" value="<?php echo $mtitle;?>" title="Meta Title" required>
      					</div>
      					<div Class="form-group field">
      						<label for="mdesc">Meta Description</label>
      						<textarea name="mdesc" rows="5" cols="25" class="form-control noresize" cols="20" rows="5" title="Meta Description" required><?php echo $mdesc;?></textarea>
      					</div>
      				</div>
          			
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <input type="submit" name="submit" value="Update" class="btn btn-primary" id="submit">
              </div>
                  <!-- /.box-footer -->
            </form>
          </div>
<script>
    // form submition for adding about details
        $("#update").submit(function(event){
            event.preventDefault(); //prevent default action 
            tinyMCE.triggerSave();
            var post_url = $(this).attr("action"); //get form action url
            var request_method = $(this).attr("method"); //get form GET/POST method
            var formData = new FormData($(this)[0]); //Encode form elements for submission
            $.ajax({
                url : post_url,
                type: request_method,
                data : formData,
                processData: false,
                contentType: false,
                beforeSend: function(){
                     $('.cssload-whirlpool').show();
                     $('.album-box').fadeTo(0,0.1);
                     $('.cssload-whirlpool1').show();
                     $('.articlesform').fadeTo(0,0.1);
                 }
            }).done(function(response){ //
                $("#result").html(response);
                $('#mtitle').val('');
                $('#title').val('');
                $('#mdesc').val('');
                $.ajax({
                    url: "views/allarticlesajax.php",
                    type: "POST",
                    data: 'data',
                    success: function(data) {
                     $('.articlesdata').html(data);
                     $('.cssload-whirlpool').delay(2000).fadeOut();
                     $('.album-box').delay(2000).fadeTo(0, 1);
                    $('.cssload-whirlpool1').delay(2000).fadeOut();
                    $('.articlesform').delay(2000).fadeTo(0, 1);
                    }
                });
                $.ajax({
                    url: "views/editarticleajax.php",
                    type: "POST",
                    data: 'data',
                    success: function(data) {
                     $('.articlesform').html(data);
                    }
                });
            
            });
        });
    // form submition for adding about details
    
    // tinymce plugin
tinymce.init({
        selector: ".t",
        theme: 'modern',
        mode : "textareas",
		plugins: [
					"advlist save emoticons autolink lists link image charmap print preview anchor",
					"searchreplace visualblocks code fullscreen tabfocus wordcount spellchecker colorpicker",
					"insertdatetime media table contextmenu textpattern paste imagetools colorpicker directionality hr textcolor toc legacyoutput"
				],
		toolbar: "leaui_formula, | styleselect, fontselect, fontsizeselect, | undo, redo, | alignleft, aligncenter, alignright, alignjustify, | bullist, numlist, indent, outdent, | print, preview, fullpage,media | forecolor, backcolor, emoticons, bold, italic, underline, strikethrough, link",
        force_br_newlines : true,
        force_p_newlines : false,
        forced_root_block : '',
    
});
  //Image preview in table row field while album update
    $(document).on('click', '#close-preview', function(){ 
        $('.image-preview').popover('hide');
        // Hover befor close the preview
        $('.image-preview').hover(
            function () {
               $('.image-preview').popover('show');
            }, 
             function () {
               $('.image-preview').popover('hide');
            }
        );    
    });
    
        // Create the close button
        var closebtn = $('<button/>', {
            type:"button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");
        // Set the popover default content
        $('.image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
            content: "There's no image",
            placement:'bottom'
        });
        // Clear event
        $('.image-preview-clear').click(function(){
            $('.image-preview').attr("data-content","").popover('hide');
            $('.image-preview-filename').val("");
            $('.image-preview-clear').hide();
            $('.image-preview-input input:file').val("");
            $(".image-preview-input-title").text("Browse"); 
        }); 
        // Create the preview image
        $(".image-preview-input input:file").change(function (){     
            var img = $('<img/>', {
                id: 'dynamic',
                width:250,
                height:200
            });      
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".image-preview-input-title").text("Change");
                $(".image-preview-clear").show();
                $(".image-preview-filename").val(file.name);            
                img.attr('src', e.target.result);
                $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
            }        
            reader.readAsDataURL(file);
        });  
// tinymce plugin
</script>
<?php }else{ ?>
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Articles</h3>
            <button class="btn btn-warning pull-right" id="add-article">Add Article</button>
            
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form id="articles_form" class="custom-form" action="ajax/articlesajax.php" method="POST" enctype="multipart/form-data">
            <div class="box-body">
                  
              <div class="col-sm-12">
  				<div Class="form-group field">
  					<label for="title">Title</label>
  					<input type="text" name="title" id="title" class="form-control" title="Article Title">
  					<input type="hidden" name="form" value="add" class="form-control" required>
					<input type="hidden" name="date" value="" id="localdate">
  				</div>
  				<div Class="form-group">
  					<label for="desc">Article Image</label>
  					<div class="input-group image-preview">
                      <input id="coverinput" type="text" class="form-control image-preview-filename" disabled="disabled" title="Please Browse an Image"> <!-- don't give a name === doesn't send on POST/GET -->
                      <span class="input-group-btn">
                          <!-- image-preview-clear button -->
                          <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                              <span class="glyphicon glyphicon-remove"></span> Clear
                          </button>
                          <!-- image-preview-input -->
                          <div class="btn btn-default field image-preview-input">
                              <span class="glyphicon glyphicon-folder-open"></span>
                              <span class="image-preview-input-title">Browse</span>
                              <input id="coverimg" type="file" accept="image/*" name="image"/> <!-- rename it -->
                          </div>
                      </span>
                  </div>
                </div>
                
  				<div Class="form-group field">
  					<label>Article Content</label>
  		            <textarea name="content" class="t" rows="10" cols="80" title="Article Content"></textarea>
  				</div>
  				<div Class="form-group field">
  					<label for="mtitle">Meta Title</label>
  					<input type="text" name="mtitle" id="mtitle" class="form-control" title="Meta Title" required>
  				</div>
  				<div Class="form-group field">
  					<label for="mdesc">Meta Description</label>
  					<textarea name="mdesc" rows="5" cols="25" class="form-control noresize" cols="20" rows="5" title="Meta Description" required></textarea>
  				</div>
  			</div>
      			
          </div>
      <!-- /.box-body -->
          <div class="box-footer">
            <input type="submit" name="submit" value="Submit" class="btn btn-primary" id="submit">
          </div>
          <!-- /.box-footer -->
          </form>
      </div>
<script>
// form submition for adding about details
    $("#articles_form").submit(function(event){
        event.preventDefault(); //prevent default action  
        tinyMCE.triggerSave();
        var post_url = $(this).attr("action"); //get form action url
        var request_method = $(this).attr("method"); //get form GET/POST method
        var formData = new FormData($(this)[0]); //Encode form elements for submission
        $.ajax({
            url : post_url,
            type: request_method,
            data : formData,
            processData: false,
            contentType: false,
            beforeSend: function(){
                 $('.cssload-whirlpool').show();
                 $('.album-box').fadeTo(0,0.1);
             }
        }).done(function(response){ //
            $("#result").html(response);
            $('#mtitle').val('');
            $('#mdesc').val('');
            $('#title').val('');
            $('#mtitle').val('');
            $('.t').val('');
            $.ajax({
                url: "views/allarticlesajax.php",
                type: "POST",
                data: 'data',
                success: function(data) {
                 $('.articlesdata').html(data);
                $('.cssload-whirlpool').delay(2000).fadeOut();
                $('.album-box').delay(2000).fadeTo(0, 1);
                }
            });
            $.ajax({
                url: "views/editarticleajax.php",
                type: "POST",
                data: 'data',
                success: function(data) {
                 $('.articlesform').html(data);
                }
            });
        
        });
    });
    // form submition for adding about details
    
    // tinymce plugin
tinymce.init({
        selector: ".t",
        theme: 'modern',
        mode : "textareas",
		plugins: [
					"advlist save emoticons autolink lists link image charmap print preview anchor",
					"searchreplace visualblocks code fullscreen tabfocus wordcount spellchecker colorpicker",
					"insertdatetime media table contextmenu textpattern paste imagetools colorpicker directionality hr textcolor toc legacyoutput"
				],
		toolbar: "leaui_formula, | styleselect, fontselect, fontsizeselect, | undo, redo, | alignleft, aligncenter, alignright, alignjustify, | bullist, numlist, indent, outdent, | print, preview, fullpage, | forecolor, backcolor, emoticons, bold, italic, underline, strikethrough, link",
        force_br_newlines : true,
        force_p_newlines : false,
        forced_root_block : '',
    
});

// tinymce plugin
    
    function editarticle(id,href){
        $.ajax({
                url: href,
                type: "POST",
                data: {edit:id},
                beforeSend: function(){
                     $('.cssload-whirlpool1').show();
                     $('.articlesform').fadeTo(0,0.1);
                 },
                success: function(data) {
                 $('.articlesform').html(data);
                 $('.cssload-whirlpool1').delay(2000).fadeOut();
                 $('.articlesform').delay(2000).fadeTo(0, 1);
                 // html Editor
                }
            });
    }
    $("#add-article").click(function(){
         var link = $(this);
         if ($('.custom-form').is(':visible')) {
             link.text('Add Album');                
        } else {
             link.text('close');                
        }        
          $(".custom-form").slideToggle();
     });
     
      //Image preview in table row field while album update
        $(document).on('click', '#close-preview', function(){ 
            $('.image-preview').popover('hide');
            // Hover befor close the preview
            $('.image-preview').hover(
                function () {
                   $('.image-preview').popover('show');
                }, 
                 function () {
                   $('.image-preview').popover('hide');
                }
            );    
        });
        // Create the close button
        var closebtn = $('<button/>', {
            type:"button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");
        // Set the popover default content
        $('.image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
            content: "There's no image",
            placement:'bottom'
        });
        // Clear event
        $('.image-preview-clear').click(function(){
            $('.image-preview').attr("data-content","").popover('hide');
            $('.image-preview-filename').val("");
            $('.image-preview-clear').hide();
            $('.image-preview-input input:file').val("");
            $(".image-preview-input-title").text("Browse"); 
        }); 
        // Create the preview image
        $(".image-preview-input input:file").change(function (){     
            var img = $('<img/>', {
                id: 'dynamic',
                width:250,
                height:200
            });      
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".image-preview-input-title").text("Change");
                $(".image-preview-clear").show();
                $(".image-preview-filename").val(file.name);            
                img.attr('src', e.target.result);
                $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
            }        
            reader.readAsDataURL(file);
        });  
</script>
<?php }?>