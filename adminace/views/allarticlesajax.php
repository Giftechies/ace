<?php require_once('../config/config.php');?>
<table id="example3" class="table table-striped table-bordered dt-responsive nowrap">
    <thead>
    <tr>
      <th>Sr.</th>
      <th>Article</th>
      <th>Image</th>
      <th>Content</th>
      <th>Video</th>
      <th>Created</th>
      <th class="text-center">Actions</th>
    </tr>
    </thead>
    <tbody>
   <?php 
        //fetch query//
          $r="select * from articles order by 1 desc";
        $sr=1;
        $result=mysqli_query($conn,$r) or die (mysql_error());
        while($row=mysqli_fetch_array($result,MYSQL_ASSOC)){
          $id=$row['id'];
        	$title=$row['title'];
          $url=$row['videourl'];
          $image=$row['image'];
        	$content=$row['content'];
        	$created=$row['created'];
        	$lastupdate=$row['lastupdate'];
        ?>
        <tr>
          <td><?php echo $sr;?></td>
          <td> <?php echo strlen($title) > 30 ? ucwords(substr($title,0,30))."..." : ucwords($title);?></td>
          <td><?php if(!empty($image)){?>
              <img src="../../images/articalimages/<?php echo $image;?>" height="50px" width="50px"></span>
              <?php } ?>
          </td>
          <td>
              <?php echo strlen($content) > 250 ? wordwrap(substr($content,0,250)."...",35,"<br>\n") : wordwrap($content,35,"<br>\n");?></span>
          </td>
          <td>
              <?php echo strlen($url) > 30 ? substr($url,0,30)."..." : $url;?></span>
          </td>
          <td>
              <span class="pull-left"><?php echo $created;?></span>
          </td>
          <td class="text-center">
      		  <button id="<?php echo $id; ?>" data-href="addalbum.php" title="Edit" onclick="editarticle(id,'views/editarticleajax.php')" class="btn btn-success edit-btn">
      		    <i class="fa fa-pencil-square-o"></i>
      		  </button>
      		  <button title="Delete" id="<?php echo $id; ?>" class="btn btn-danger delete" data-href="articles.php?del=" data-toggle="modal" data-target="#myModal" onclick="deleteit(id,'articles.php?del=');"><i class="fa fa-times"></i></button>
      	  </td>
        </tr>
    <?php $sr++;}?>

    </tbody>
</table>
<script>
  // initializing data table
    $('#example3').DataTable({ 
      columnDefs: [
        { responsivePriority: 1, targets: 0 },
        { responsivePriority: 2, targets: -1 }
     ],
      "drawCallback": function( settings ) {

            // popover
            $('[data-toggle="popover"]').popover({
                placement : 'top',
                trigger : 'hover'
            }); 
            
            //delete confirm popup
            function deleteit(id,href){
            var id= id;
            var href= href;
            $('.del-confirm').attr('href',href+id);
            }
            //delete confirm popup

        },
    });
    // initializing data table
    
    //  image delete action
    function deleteit(id,href){
    var id= id;
    var href= href;
    $('.del-confirm').attr('href',href+id);
    }
     //  image delete action
</script>