<?php
require_once('../config/config.php');
require_once('../config/functions.php');
function create_slug($string){
	   $string1=str_replace('-', ' ', $string);
	   $slug=strtolower(preg_replace('/[^A-Za-z0-9-]+/', '-', $string1));
	   return $slug;
	}
if($_POST['form']=='add'){
		if($_POST['title']=="")
		{
		    $msg="<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
							<span class=\"bold\">Error: </span>  Please Fill the Title </div>";
		}
		elseif($_POST['content']=="")
		{
		    $msg="<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
							<span class=\"bold\">Error: </span>  Please Add Content </div>";
		}elseif($_POST['mtitle']=="")
		{
		    $msg="<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
							<span class=\"bold\">Error: </span>  Please Fill the Meta Title Description </div>";
		}elseif($_POST['mdesc']=="")
		{
		    $msg="<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
							<span class=\"bold\">Error: </span>  Please Fill the Meta Description </div>";
		}else{
			$file=$_FILES['image']['name'];
			$str = '';
			$str_len = 4 ;
			for($i = 0; $i < $str_len; $i++){
			    //97 is ascii code for 'a' and 122 is ascii code for z
			    $str .= chr(rand(97, 122));
			}
			$temp = explode(".", $file);
			$extension = end($temp);
			if(!empty($file)){
				$filenew=$temp[0] .$str.".".$extension;
			}else{
				$filenew="";
			}
				$slug=create_slug($_POST['title']);
			$p="insert into articles set
			    title='".mysqli_real_escape_string($conn,$_POST['title'])."',
			    image='".mysqli_real_escape_string($conn,$filenew)."',
			    content='".mysqli_real_escape_string($conn,$_POST['content'])."',
			    mtitle='".mysqli_real_escape_string($conn,$_POST['mtitle'])."',
			    mdesc='".mysqli_real_escape_string($conn,$_POST['mdesc'])."',
			    slug='".mysqli_real_escape_string($conn,$slug)."',
			    created='".mysqli_real_escape_string($conn,$_POST['date'])."'
			    ";
			$n=mysqli_query($conn,$p);
			if($n){ 
				move_uploaded_file($_FILES['image']['tmp_name'],'../../images/articalimages/'.$filenew);
			if(!empty($filenew)){	createThumbnail($filenew,'../../images/articalimages/','../../images/articalthumbs/',284,177);}
				$msg="<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
							<span class=\"bold\">Success: </span>  Article Added Successfully. </div>";
			 }
			else{$msg="<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
							<span class=\"bold\">Error: </span>  Sorry Failed. </div>"; }
		}
	echo $msg;
}
elseif($_POST['form']=='update'){
		if($_POST['title']=="")
		{
		    $msg="<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
							<span class=\"bold\">Error: </span>  Please Fill the Title </div>";
		}
		elseif($_POST['content']=="")
		{
		    $msg="<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
							<span class=\"bold\">Error: </span>  Please Add Content </div>";
		}elseif($_POST['mtitle']=="")
		{
		    $msg="<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
							<span class=\"bold\">Error: </span>  Please Fill the Meta Title Description </div>";
		}elseif($_POST['mdesc']=="")
		{
		    $msg="<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
							<span class=\"bold\">Error: </span>  Please Fill the Meta Description </div>";
		}else{
			if($_FILES['image']['name']==""){
				$slug=create_slug($_POST['title']);
				$p="update articles set
			    title='".mysqli_real_escape_string($conn,$_POST['title'])."',
			    content='".mysqli_real_escape_string($conn,$_POST['content'])."',
			    mtitle='".mysqli_real_escape_string($conn,$_POST['mtitle'])."',
			    mdesc='".mysqli_real_escape_string($conn,$_POST['mdesc'])."',
			    slug='".mysqli_real_escape_string($conn,$slug)."',
			    lastupdate='".mysqli_real_escape_string($conn,$_POST['date'])."'
			    where id='".mysqli_real_escape_string($conn,$_POST['id'])."'";
				$n=mysqli_query($conn,$p);
				if($n){ 
					$msg="<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
								<span class=\"bold\">Success: </span>  Article Updated Successfully. </div>";
				 }
				else{
					$msg="<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
								<span class=\"bold\">Error: </span>  Sorry Failed. </div>"; }
			
			}else{
			$file=$_FILES['image']['name'];
			$str = '';
			$str_len = 4 ;
			for($i = 0; $i < $str_len; $i++){
			    //97 is ascii code for 'a' and 122 is ascii code for z
			    $str .= chr(rand(97, 122));
			}
			$new=$_POST['oldimg'];
			$temp = explode(".", $file);
			$extension = end($temp);
			$filenew=$temp[0] .$str.".".$extension;
			$slug=create_slug($_POST['title']);
			$p="update articles set
			    title='".mysqli_real_escape_string($conn,$_POST['title'])."',
			    image='".mysqli_real_escape_string($conn,$filenew)."',
			    content='".mysqli_real_escape_string($conn,$_POST['content'])."',
			    mtitle='".mysqli_real_escape_string($conn,$_POST['mtitle'])."',
			    mdesc='".mysqli_real_escape_string($conn,$_POST['mdesc'])."',
			    slug='".mysqli_real_escape_string($conn,$slug)."',
			    lastupdate='".mysqli_real_escape_string($conn,$_POST['date'])."'
			    where id='".mysqli_real_escape_string($conn,$_POST['id'])."'";
			$n=mysqli_query($conn,$p);
			if($n){ 
				unlink('../../images/articalimages/'.$new);
				unlink('../../images/articalthumbs/'.$new);
				move_uploaded_file($_FILES['image']['tmp_name'],'../../images/articalimages/'.$filenew);
				if(!empty($filenew)){	createThumbnail($filenew,'../../images/articalimages/','../../images/articalthumbs/',284,177);}
				$msg="<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
							<span class=\"bold\">Success: </span>  Article Updated Successfully. </div>";
			 }
			else{$msg="<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
							<span class=\"bold\">Error: </span>  Sorry Failed. </div>"; }
		}
	  }
	echo $msg;
}			
?>