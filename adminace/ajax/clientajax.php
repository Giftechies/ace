
<?php 
require_once('../config/config.php');
require_once('../config/functions.php');
		// insert query into albums
		if($_POST['form']=="addclient"){
			if($_POST['title']=="")
			{
				$msg="<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
								<span class=\"bold\">Error: </span>  Please Fill the Title. </div>";
			}elseif($_FILES['image']['name']=="")
			{
			    $msg="<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
								<span class=\"bold\">Error: </span>  Please Attach the Client Image </div>";
			}else{
				$p1="select * from ourclients where title='".mysqli_real_escape_string($conn,$_POST['title'])."'";
				 $n=mysqli_query($conn,$p1);
				 if(mysqli_num_rows($n)>0){
					 $msg="<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
				    							<span class=\"bold\">Error: </span> Title Already Exist. </div>"; 
				 }else{
				 	$file=$_FILES['image']['name'];
					$str = '';
					$str_len = 4 ;
					for($i = 0; $i < $str_len; $i++){
					    //97 is ascii code for 'a' and 122 is ascii code for z
					    $str .= chr(rand(97, 122));
					}
					$temp = explode(".", $file);
					$extension = end($temp);
					if(!empty($file)){
						$filenew=$temp[0] .$str.".".$extension;
					}else{
						$filenew="";
					}
						$p="insert into ourclients set
						    title='".mysqli_real_escape_string($conn,$_POST['title'])."',
						    image='".mysqli_real_escape_string($conn,$filenew)."',
						    created='".mysqli_real_escape_string($conn,$_POST['date'])."'
						    ";
						    if(move_uploaded_file($_FILES['image']['tmp_name'],'../../images/clients/'.$filenew)){
						    	createThumbnail($filenew,'../../images/clients/','../../images/clientsthumbs/');
							    $n=mysqli_query($conn,$p);
				    			if($n){ 
				    			  $msg="<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
				    							<span class=\"bold\">Success: </span>  Client Added Successfully. </div>";
				    			 }
				    			else{
				    			  $msg="<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
				    							<span class=\"bold\">Error: </span>  Sorry Failed. </div>"; 
				    			  
				    			}
						    }else{
						      $msg="<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
				    							<span class=\"bold\">Error: </span>Sorry Upload Error. </div>";
						    }
				 }
			}
		echo $msg; // Update query into albums
		}elseif($_POST['form']=="updateclient"){
				$id=mysqli_real_escape_string($conn,$_POST['id']);
				$img=mysqli_real_escape_string($conn,$_POST['imgold']);
			if($_POST['title']=="")
			{
				$msg="<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
								<span class=\"bold\">Error: </span>  Please Fill the Title. </div>";
			}
			elseif($_FILES['image']['name']=="" || $_FILES['image']['name']==null)
			{
			    $p="update ourclients set
				    title='".mysqli_real_escape_string($conn,$_POST['title'])."',
				    lastupdate='".mysqli_real_escape_string($conn,$_POST['date'])."'
				    where id=$id
				    ";
				    $n=mysqli_query($conn,$p);
	    			if($n){ 
	    			  $msg="<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
	    							<span class=\"bold\">Success: </span>  Client Information Updated Successfully.</div>";
	    			 }
	    			else{
	    			  $msg="<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
	    							<span class=\"bold\">Error: </span>  Sorry Failed. </div>"; 
	    			}
			}else{
				$file=$_FILES['image']['name'];
					$str = '';
					$str_len = 4 ;
					for($i = 0; $i < $str_len; $i++){
					    //97 is ascii code for 'a' and 122 is ascii code for z
					    $str .= chr(rand(97, 122));
					}
					$temp = explode(".", $file);
					$extension = end($temp);
					if(!empty($file)){
						$filenew=$temp[0] .$str.".".$extension;
					}else{
						$filenew="";
					}
					$p="update ourclients set
					    title='".mysqli_real_escape_string($conn,$_POST['title'])."',
					    image='".mysqli_real_escape_string($conn,$filenew)."',
					    lastupdate='".mysqli_real_escape_string($conn,$_POST['date'])."'
					    where id=$id
					    ";
					    if(move_uploaded_file($_FILES['image']['tmp_name'],'../../images/clients/'.$filenew)){
					    	createThumbnail($filenew,'../../images/clients/','../../images/clientsthumbs/');
					    	unlink('../../images/clients/'.$img);
					    	unlink('../../images/clientsthumbs/'.$img);
		    			    $n=mysqli_query($conn,$p);
		        			if($n){ 
		        			  $msg="<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
		        							<span class=\"bold\">Success: </span>  Client Information Updated Successfully. </div>";
		        			 }
		        			else{
		        			  $msg="<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
		        							<span class=\"bold\">Error: </span>  Sorry Failed. </div>"; 
		        			  
		        			}
					    }else{
					      $msg="<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
		        							<span class=\"bold\">Error: </span>Sorry Upload Error. </div>";
					    }
			}
		echo $msg; 
		}
		
?>