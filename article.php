<?php include_once 'common-files/header.php'; ?>
<?php require_once('common-files/functions.php');?>
     
</div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 inner-articles-banner-img">
            <div class="banner-blur-background"></div>
            <span class="inner-articles-heading">Articles</span>
        </div>
    </div>
    <div class="row alll-articl-bottom-padding">
        <?php
            $id=mysqli_real_escape_string($conn,$_GET['id']);
            $q1="select * from articles where id='$id'"; 
            $run1=mysqli_query($conn,$q1);
            while($row=mysqli_fetch_array($run1)){
            $title=$row['title'];
            $videolink=$row['videourl'];
            $ytarray=explode("/", $videolink);
            $ytendstring=end($ytarray);
            $ytendarray=explode("?v=", $ytendstring);
            $ytendstring=end($ytendarray);
            $ytendarray=explode("&", $ytendstring);
            $ytcode=$ytendarray[0];
            $url= "https://www.youtube.com/embed/".$ytcode;
            $date=new DateTime($row['created']);
        ?>
        <?php if(!empty($row['image'])){?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 inner-article-img-padding">
            <img class="img-responsive center-block" src="<?php echo SITE_PATH;?>/images/articalimages/<?php echo $row['image'];?>" alt="Article Image" title="Article Image" />
        </div>
        <?php }?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1 class="inner-heading-title"><?php echo strlen($row['title']) > 80 ? substr($row['title'],0,80)."..." : $row['title'];?></h1>
            <span class="inner-articl-date pull-right"><?php echo date_format($date, 'd-m-Y');?></span>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <p class="inner-detail-articls">
                <?php echo $row['content'];?> 
            </p>
        </div>
        <?php } ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 sililar-posts-heading">
            <span class="similar-posts">YOU MAY ALSO LIKE</span>
            <hr>
        </div>
        <?php 
             $q1="select * from articles order by rand() LIMIT 0, 6"; 
              $run1=mysqli_query($conn,$q1);
              while($row=mysqli_fetch_array($run1)){
                 $date=new DateTime($row['created']);
                 ?>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
                <div class="bottom-brdr1">
                    <div class="row article-padding-bottom">
                        <?php if(!empty($row['image'])){?>
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                            <img class="img-responsive center-block" src="<?php echo SITE_PATH;?>/images/articalthumbs/<?php echo $row['image'];?>" alt="Article Image" title="<?php echo $row['title'];?>" />
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                            <a href="<?php echo SITE_PATH.'/article.php?id='.$row['id'];?>" class="article-news-title">
                                <?php echo strlen($row['title']) > 75 ? substr($row['title'],0,75)."..." : $row['title'];?>
                            </a>
                            <span class="articles-dates"><?php echo date_format($date, 'd-m-Y');?></span>
                        </div>
                        <?php }else{ ?>
                        <div class="col-lg-12 col-md-12 col-sm-2 col-xs-12">
                            <a href="<?php echo SITE_PATH.'/article.php?id='.$row['id'];?>" class="article-news-title">
                                <?php echo strlen($row['title']) > 150 ? substr($row['title'],0,150)."..." : $row['title'];?>
                            </a>
                            <span class="articles-dates"><?php echo date_format($date, 'd-m-Y');?></span>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php } ?>
        
    </div><!--row div ends-->
    
<?php include_once 'common-files/footer.php'; ?>