$(document).ready(function(){
    $('a.various').fancybox({
        width: 640,
        height: 400,
        type: 'iframe'
    });
    
    $(".fancybox").fancybox({
		openEffect  : 'none',
		closeEffect : 'none',
		iframe : {
			preload: false
		}
	});
	
	$('.fancybox-media').fancybox({
		openEffect  : 'none',
		closeEffect : 'none',
		helpers : {
			media : {}
		}
	});
	
    $('#demo_box').popmenu({
        'controller': true,   // use control button or not
        'width': '100%',         // width of menu
        'background': '#2E8AD7',  // background color of menu
        'focusColor': '#125A95',  // hover color of menu's buttons
        'borderRadius': '10px',   // radian of angles, '0' for right angle
        'top': '50',              // pixels that move up
        'left': '0',              // pixels that move left
        'iconSize': '33%'    
    });
    
    $('.bxslider').bxSlider({
        minSlides: 1,
        maxSlides: 8,
        slideWidth: 120,
        slideMargin: 10,
        auto: true,
        moveSlides: 8,
        pager: false,
        responsive: true,
        controls: false,
        autoHover: true,
       
    });
    
    $(".select-no").select2();
        $( function() { $( 'audio' ).audioPlayer(); } );
        
});