
/*$(window).on('load', function() {
    $(".puls-img-loader").fadeOut(1000);
	$(".loading").fadeOut(2500);
});*/

/*chng classesa on responsive*/
function chngclass(){
    var windowwidth = $(window).width();
    if(windowwidth >480 && windowwidth <768){
        $(".width-function").removeClass("col-xs-12");
        $(".width-function").addClass("col-xs-6");
        $(".mobile-full-article").removeClass("col-xs-12");
        $(".mobile-full-article").addClass("col-xs-6");
        $(".live-tv-img1").removeClass("col-xs-12");
        $(".live-tv-img1").addClass("col-xs-6");
        $(".live-tv-img1").addClass("col-xs-offset-3");
        $(".album-padding").removeClass("col-xs-12");
        $(".album-padding").addClass("col-xs-6");
    }
    else{
        $(".width-function").removeClass("col-xs-6");
        $(".width-function").addClass("col-xs-12");
        $(".mobile-full-article").removeClass("col-xs-6");
        $(".mobile-full-article").addClass("col-xs-12");
        $(".live-tv-img1").addClass("col-xs-12");
        $(".live-tv-img1").removeClass("col-xs-6");
        $(".live-tv-img1").removeClass("col-xs-offset-3");
        $(".album-padding").removeClass("col-xs-6");
        $(".album-padding").addClass("col-xs-12");
    }
}

/*navbar fixed*/
function mnavbar(){
        $(window).scroll(function(){ 
        if ($(this).scrollTop() > 30) { 
            $('.navbtn-fixed').css({"position":"fixed","width":"100%","transition": "0.6s ease","top":"0","z-index":"9999","background-color":"#fff","padding":"5px 5px 20px"}); 
        } else { 
            $('.navbtn-fixed').css({"position":"relative","background":"#fff","padding":"0px"}); 
        }
    });
 }
 
/*mobile view navbar */
function hidemnavbar(){
    $(".btn-dlod").click(function(e){
        $(".demo_li").addClass("rdlod");
        $(".download-icons").addClass("navstore");
        $(".back-icon").addClass("back-icon-width");
        e.preventDefault();
    });
    $(".fa-arrow-circle-left").click(function(e){
        $(".demo_li").removeClass("rdlod");
        $(".download-icons").removeClass("navstore");
        $(".back-icon").removeClass("back-icon-width");
        e.preventDefault();
    });
    $(".fa-bars").click(function(e){
        $(".demo_li").removeClass("rdlod");
        $(".download-icons").removeClass("navstore");
        $(".back-icon").removeClass("back-icon-width");
        e.preventDefault();
    });
}

/*form validations starts*/
function validator(){
    
    /*message for error messages*/
    var FirstNameMessage ="Please enter your name";
    var EmailMessage ="Invalid Email";
    var MessageBOxMessage ="Minimum 140 Words";
    
     /*===============first name input feild==============*/
    $("#name").blur(function(){
          var slector ="#name";
          var firstname = $("#name").val();
           if(firstname.length > 2){
              $("#submit").prop('disabled', false);
               RemoveErrorMessage(slector);
           }
           else{
                $("#submit").prop('disabled', true);
               showErrorMessage(FirstNameMessage,slector);
           }
    });
    
    
    /*=================Email Address Input feild:=============*/
     $("#email").blur(function(){
        var slector ="#email";
        var email = $("#email").val();
        var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
            if (filter.test(email)) {
            $("#submit").prop('disabled', false);
            RemoveErrorMessage(slector);
            }
            else {
                 $("#submit").prop('disabled', true);
             showErrorMessage(EmailMessage,slector);
            }
    });     
    
    /*==================message text area input==================*/
     $("#message").blur(function(){
        var slector ="#message";
        var messages = $("#message").val();
           if(messages.length > 35){
               RemoveErrorMessage(slector);
               $("#submit").prop('disabled', false);
           }
           else{
                $("#submit").prop('disabled', true);
               showErrorMessage(MessageBOxMessage,slector);
               
           }
      });
      
      /*required no*/
      $('#land-line-no , #mobile-number').keydown(function(event){
        // Allow special chars + arrows 
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 
            || event.keyCode == 27 || event.keyCode == 13 
            || (event.keyCode == 65 && event.ctrlKey === true) 
            || (event.keyCode >= 35 && event.keyCode <= 39)){
                return;
        }else {
        // If it's not a number stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault(); 
                }   
            }
    });
      
    /*function check input feild on submit*/
    $("#contact_form").submit(function(e){
        var count=true;
        var submitMessage ="Feild is required !";
        var firstname = $("#name").val();
        var email =  $("#email").val();
        var message = $("#message").val();
        var selector="#contact_form";
          
        /*__________first name validate__________*/
        if(firstname.length > 2){
            count=false;
            RemoveErrorMessage("#name");
        }
        else{
            count=true;
            showErrorMessage(FirstNameMessage,"#name");
        }
           
        /*__________email validate_____________*/
        var emailfilter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
            if (emailfilter.test(email)) {
                count=false;
                RemoveErrorMessage("#email");
            }
            else {
                count=true;
                showErrorMessage(EmailMessage,"#email");
            }
        if(message.length > 35){
            count=false;
            RemoveErrorMessage("#message");
        }
        else{
            count=true;
            showErrorMessage(MessageBOxMessage,"#message");
        }
        if(count){
            e.preventDefault();
        }
          
});   
}
/*=============function for add Error message=====================*/

function showErrorMessage(message,slector){
   
    var message ="<div class='remove'><div class='popup'>"+message+"</div><span class='triangle-down'></span><div>";
    $(''+slector+'').before(message);
}

/*=============function for Remove Error message=====================*/
function RemoveErrorMessage(slector){
    $(''+slector+'').parent().find(".remove").remove();
}
/*form validations ends*/

/* gallery navbar */
 function navtab(){
    $(window).scroll(function(){ 
        var wh = $(window).width();
        if ($(this).scrollTop() > 30 && wh <767) { 
            $('.albums-navbar').css({"position":"fixed","transition": "0.6s ease","top":"80px","left":"0","z-index":"999","background-color":"#fff","margin-bottom":"10px"}); 
        } 
        else if($(this).scrollTop() > 30 && wh >768){
            $('.albums-navbar').css({"position":"fixed","transition": "0.6s ease","top":"0px","z-index":"999","background-color":"#fff","margin-bottom":"10px"}); 
        }
        else { 
            $('.albums-navbar').css({"position":"relative","background":"none","top":"auto"}); 
        } 
    }); 
 }
 
  function aimage(id) {
        $(".albumimage").show();
        $("#returnimages").html('');
        $("#onealbum").show();
        $(".albumhide").hide();
        filter(id);
    }
        
    function goback() {
        $(".albumimage").hide();
        $(".albumhide").show();
        $("#onealbum").hide();
    }
    function filter(a){
             var id=a;
            
            $.ajax({
                 
                url: "galleryajax.php",
                data: {id:id},
                 type: "POST",
                
                success: function(data) { 
                $('.returnimages').html(data);
                $(".rowalbumimage").val("albumimageval"); 
                $(".rowalbum").val("");
                $(".rowimage").val("");
                }
            });
            return false;
    }

/*$(document).on('click', '#close-preview', function(){ 
        $('.image-preview').popover('hide');
        // Hover befor close the preview
        $('.image-preview').hover(
            function () {
               $('.image-preview').popover('show');
            }, 
             function () {
               $('.image-preview').popover('hide');
            }
        );    
    });
    
    $(function() {
        // Create the close button
        var closebtn = $('<button/>', {
            type:"button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");
        // Set the popover default content
        $('.image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
            content: "There's no image",
            placement:'bottom'
        });
        // Clear event
        $('.image-preview-clear').click(function(){
            $('.image-preview').attr("data-content","").popover('hide');
            $('.image-preview-filename').val("");
            $('.image-preview-clear').hide();
            $('.image-preview-input input:file').val("");
            $(".image-preview-input-title").text("Browse"); 
        }); 
        // Create the preview image
        $(".image-preview-input input:file").change(function (){     
            var img = $('<img/>', {
                id: 'dynamic',
                width:250,
                height:200
            });      
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".image-preview-input-title").text("Change");
                $(".image-preview-clear").show();
                $(".image-preview-filename").val(file.name);            
                img.attr('src', e.target.result);
                $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
            }        
            reader.readAsDataURL(file);
        });  
    });*/
    
   