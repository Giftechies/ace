$(document).ready(function(){
   hidemnavbar(); 
   mnavbar();
   validator();
   navtab();
   chngclass(); 
   $('.leftSidebar, .content')		
   .theiaStickySidebar({		
      additionalMarginTop: 0	
   });
});

$(window).load(function(){
   chngclass(); 
});

$(window).resize(function(){
   chngclass(); 
   navtab();
});