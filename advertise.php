<?php include_once 'common-files/header.php'; ?>
</div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 advertise-banner-img">
            <div class="banner-blur-background"></div>
            <h1 class="advertise-heading">Advertisers</h1>
        </div>
        <?php $q="select * from ourclients";
            $run=mysqli_query($conn,$q);
            while($row=mysqli_fetch_array($run)){
        ?>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 album-padding">
            <div class="gallery-images">
                <a class="fancybox" href="<?php echo SITE_PATH;?>/images/clients/<?php echo $row['image'];?>" data-fancybox-group="gallery" >
                    <div class="view second-effect">
                        <img class="img-responsive center-block" src="<?php echo SITE_PATH;?>/images/clientsthumbs/<?php echo $row['image'];?>" alt="Image" title="Image" />
                        <div class="mask">
                            <i class="fa fa-search-plus info" aria-hidden="true"></i>
                    	</div>
                	</div>
                </a>
                <span class="gallery-image-title"> <?php echo $row['title'];?> </span>
            </div>
        </div>
        <?php } ?>
    </div>
<?php include_once 'common-files/footer.php'; ?>