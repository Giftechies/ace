<?php include_once 'common-files/header.php'; ?>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 add-with-us-banner-img">
            <div class="banner-blur-background"></div>
            <h1 class="add-with-us-heading">Advertise With Us</h1>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form id="advertise-form" action="#" name="contactform" method="post" data-effect="slide-bottom">
                	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    	<input type="text" name="name" id="name" class="form-control name margin-bottom" placeholder="Name"> 
                    </div>
                	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    	<input type="text" name="email" id="email" class="form-control email margin-bottom" placeholder="Email Address"> 
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                    	<select class="select-no margin-bottom">
                            <option value="AL">Alabama</option>
                            <option value="WY">Wyoming</option>
                            <option value="yy">Wyoming</option>
                        </select>
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-6 no-padding area-code-padding">
                        <input type="text" name="area-code" id="area-code" class="form-control area-code margin-bottom" placeholder="Area Code">
                    </div>
                	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    	<input type="text" name="land-line-no" id="land-line-no" class="form-control land-line-no margin-bottom" placeholder="Land Line"> 
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    	<select class="select-no margin-bottom">
                            <option value="AL">Alabama</option>
                            <option value="WY">Wyoming</option>
                            <option value="yy">Wyoming</option>
                        </select>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    	<input type="text" name="mobile-number" pattern="[789][0-9]{9}" id="mobile-number" class="form-control margin-bottom mobile-number" placeholder="Mobile Number"> 
                    </div>
                    <!--<div Class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    					<label for="desc">Article Image</label>
    					<div class="input-group image-preview">
                            <input id="coverinput" type="text" class="form-control image-preview-filename" disabled="disabled" title="Please Browse an Image">
                            <span class="input-group-btn">
                                <!-- image-preview-clear button -->
                                <!--<button type="button" class="btn btn-default image-preview-clear" style="display:none;">-->
                                <!--    <span class="glyphicon glyphicon-remove"></span> Clear-->
                                <!--</button>-->
                              <!-- image-preview-input -->
                                <!--<div class="btn btn-default field image-preview-input">
                                    <span class="glyphicon glyphicon-folder-open"></span>
                                    <span class="image-preview-input-title">Browse</span>
                                    <input id="coverimg" type="file" accept="image/*" name="image"/> <!-- rename it -->
                                <!--</div>
                            </span>
                       </div>
                    </div>-->
    				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
    					<button type="submit" value="SEND" id="submit" class="btn btn-lg btn-primary margin-bottom">SEND</button>
    				</div>
    			</form>
    			
                
        </div>
    </div>
<?php include_once 'common-files/footer.php'; ?>