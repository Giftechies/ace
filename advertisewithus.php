<?php include_once 'common-files/header.php'; ?>
</div>

<section class="postwrapper clearfix">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 contact-us-banner-img">
            <div class="banner-blur-background"></div>
            <h1 class="contact-us-heading">ADVERTISE WITH US</h1>
        </div>
       <!-- <div Class="form-group">
					<label for="desc">Article Image</label>
					<div class="input-group image-preview">
              <input id="coverinput" type="text" class="form-control image-preview-filename" disabled="disabled" title="Please Browse an Image">
              <span class="input-group-btn">
                  <!-- image-preview-clear button -->
                  <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                      <span class="glyphicon glyphicon-remove"></span> Clear
                  </button>
                  <!-- image-preview-input -->
                  <!--<div class="btn btn-default field image-preview-input">
                      <span class="glyphicon glyphicon-folder-open"></span>
                      <span class="image-preview-input-title">Browse</span>
                      <input id="coverimg" type="file" accept="image/*" name="image"/>
                  </div>-->
              </span>
          </div>
        </div>-->
        <?php $q="select * from contacts order by 1 desc limit 1";
                $run=mysqli_query($conn,$q);
                while($row=mysqli_fetch_array($run)){
                    $address=$row['address'];
                    $mobile=$row['mobile'];
                    $phone=$row['phone'];
                    $email=$row['email'];
                    $country=$row['country'];
                	$state=$row['state'];
                	$city=$row['city'];
                  	$p1="select * from countries where id=$country";
                  	$run1=mysqli_query($conn,$p1);
                    while($row1=mysqli_fetch_array($run1)){
                      $countryname=$row1['name'];
                    }
                    $p2="select * from states where id=$state";
                  	$run2=mysqli_query($conn,$p2);
                    while($row2=mysqli_fetch_array($run2)){
                      $statename=$row2['name'];
                    }
                    $p3="select * from cities where id=$city";
                  	$run3=mysqli_query($conn,$p3);
                    while($row3=mysqli_fetch_array($run3)){
                      $cityname=$row3['name'];
                    }
                	
                	$pin=$row['pin'];
                }
        ?>
        
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 form-padding">
            <div class="drop-us-a-line text-center">
            </div>
            <div class="clearfix"></div>
			<form id="contactform" action="#" name="contactform" method="post" data-effect="slide-bottom">
            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                	<input type="text" name="name" id="name" class="form-control name" placeholder="Name"> 
                </div>
            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                	<input type="text" name="email" id="email" class="form-control email" placeholder="Email Address"> 
                </div>
            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                	<input type="text" name="title" id="title" class="form-control title" placeholder="Title"> 
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                	<input type="text" name="subject" id="subject" class="form-control subject" placeholder="Subject"> 
                </div>
                
                <div class="clearfix"></div>
                
            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                	<textarea class="form-control Message" name="comments" id="message" rows="6" placeholder="Message"></textarea>
                </div>
                
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
					<button type="submit" value="SEND" id="submit" class="btn btn-lg btn-primary">SEND</button>
				</div>
			</form>      
                   
        </div><!--form end-->
        
    </div><!-- end row -->
</section><!-- end postwrapper -->

<?php include_once 'common-files/footer.php'; ?>
