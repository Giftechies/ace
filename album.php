<?php include_once 'common-files/header.php'; ?>
<?php require_once('common-files/functions.php');?>
        
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about-us-banner-img">
        <div class="banner-blur-background"></div>
        <h1 class="about-us-heading">Albums</h1>
    </div>
    
    <div class="col-xs-12 gallerydiv">
        <div class="col-xs-12 backbtn">
            <a href="<?php echo SITE_PATH."/gallery";?>" class="pull-right btn btn-warning">Back to Gallery</a>
        </div>
      <?php if(isset($_GET['slug'])){?>
        <?php 
        $slug=$_GET['slug'];
        $q1="select * from albums where slug='$slug'";
        $run1=mysqli_query($conn,$q1);
        if(mysqli_num_rows($run1)>0){
            while($row1=mysqli_fetch_array($run1)){
                $id=$row1['id'];
                $q2="select * from albumimages where parent_id='$id'";
                if(isset($_GET["page"])){ //Get page number from $_GET["page"]
                    $page_number = filter_var($_GET["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
                    if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
                }else{
                    $page_number = 1; //if there's no page number, set it to 1
                }
                $page_url=SITE_PATH."/album/".$_GET['slug'];
                    $q="select * from albumimages where parent_id='$id' order by 1 desc";
                      $run=mysqli_query($conn,$q);
                      $item_per_page=12;
                      $page_position = (($page_number-1) * $item_per_page);
                      $get_total_rows=mysqli_num_rows($run);
                      $total_pages = ceil($get_total_rows/$item_per_page);
                      $q3="select * from albumimages where parent_id='$id' order by 1 desc LIMIT $page_position, $item_per_page"; 
                      $run3=mysqli_query($conn,$q3);
                      if(mysqli_num_rows($run3)>0){
                      while($row3=mysqli_fetch_array($run3)){ ?>
                      
                          <div class="col-xs-6 col-sm-4 col-md-3">            
                            <div class="thumbnail">
                                <div class="caption">
                                    <p class="viewimagebtn">
                                        <a class="fancybox label label-danger" rel="myGallery" href="<?php echo SITE_PATH;?>/images/albumimages/<?php echo $row3['image'];?>">
                                            view
                                        </a>
                                    </p>
                                </div>
                                <img src="<?php echo SITE_PATH;?>/images/albumimagesthumbs/<?php echo $row3['image'];?>" alt="...">
                            </div>  
                        </div>
                 <?php    }
            }else{
                    echo "<pre>Sorry No Images inside Album</pre>";
                }
            }
        }else{
            echo "<pre>Sorry No Images inside Album</pre>";
        }?>
   <?php } ?>
       
        </div>
        <?php echo '<div class="col-sm-12 text-center">';
            // We call the pagination function here. 
            echo paginate($item_per_page, $page_number, $get_total_rows[0], $total_pages, $page_url);
            echo '</div>';
        ?>
                 
    </div>
    
<?php include_once 'common-files/footer.php'; ?>
