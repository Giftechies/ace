                    
                </div><!--column-9s closing div-->
            </div><!--rows closing div-->
        </div><!--container-fluids closing-div-->
        <!--script files-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        
        <script src="https://maps.googleapis.com/maps/api/js?v=3&amp;sensor=false&amp;libraries=places"></script>
        <script src="<?php echo SITE_PATH;?>/js/map.js"></script>
        
        <!-- Add jQuery library -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        
        <!--mobile navbar scripts-->
        <script src="<?php echo SITE_PATH;?>/jQueryPopMenu/lib/jquery/jquery.min.js"></script>
        <script src="<?php echo SITE_PATH;?>/jQueryPopMenu/src/jquery.popmenu.js"></script>
        
        <!--mp3 player script-->
        <script src="<?php echo SITE_PATH;?>/AudioPlayer/AudioPlayer/js/audioplayer.js"></script>
        
        <!--fancybox scripts-->
        <script type="text/javascript" src="<?php echo SITE_PATH;?>/fancybox/source/jquery.fancybox.js"></script>
        <script type="text/javascript" src="<?php echo SITE_PATH;?>/fancybox/source/jquery.fancybox.pack.js"></script>
        <script type="text/javascript" src="<?php echo SITE_PATH;?>/fancybox/source/helpers/jquery.fancybox-buttons.js"></script>
        <script type="text/javascript" src="<?php echo SITE_PATH;?>/fancybox/source/helpers/jquery.fancybox-media.js"></script>
        <script type="text/javascript" src="<?php echo SITE_PATH;?>/fancybox/source/helpers/jquery.fancybox-thumbs.js"></script>
        <script type="text/javascript" src="<?php echo SITE_PATH;?>/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>	
        
        <!--bx-slider-->
        <script type="text/javascript" src="<?php echo SITE_PATH;?>/jquery.bxslider/jquery.bxslider.min.js"></script>
        
        <!--select2-->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.min.js"></script>
        
        <!--function or call scripts-->
        <script type="text/javascript" src="<?php echo SITE_PATH;?>/js/function.js"></script>
        <script type="text/javascript" src="<?php echo SITE_PATH;?>/js/call.js"></script>
        <script type="text/javascript" src="theia-sticky-sidebar-master/js/theia-sticky-sidebar.js"></script>
        <script type="text/javascript" src="<?php echo SITE_PATH;?>/js/plugins/plugins.js"></script>

</body>
</html>