
<?php require_once('adminace/config/config.php');?>
<!DOCTYPE html> 
<html>
    <head>
        <title>ACE</title>
        
        <!--fav-icons-->
        <link rel="icon" href="<?php echo SITE_PATH;?>/images/logo.png" type="images/png" sizes="16*16">
        
        <meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        
        <!--fontawesome-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
        
        <!--font effects-->
        <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet" />
        
        <!--mp3 player css-->
        <link rel="stylesheet" href="<?php echo SITE_PATH;?>/AudioPlayer/AudioPlayer/css/audioplayer.css" />
        
        <!--fancybox css-->
        <link rel="stylesheet" href="<?php echo SITE_PATH;?>/fancybox/source/jquery.fancybox.css" />
        <link rel="stylesheet" href="<?php echo SITE_PATH;?>/fancybox/source/helpers/jquery.fancybox-buttons.css" />
        <link rel="stylesheet" href="<?php echo SITE_PATH;?>/fancybox/source/helpers/jquery.fancybox-thumbs.css" />
        
        <!--bxslider-->
        <link rel="stylesheet" href="<?php echo SITE_PATH;?>/jquery.bxslider/jquery.bxslider.css" />
        
        <!--select2 css-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" />
        
        <!--stylecss file-->
        <link rel="stylesheet" href="<?php echo SITE_PATH;?>/css/style.css" />
        
    </head>
    <body >
        <div class="loading">
            <svg class="puls-img-loader" version="1.2" height="300" width="600" xmlns="http://www.w3.org/2000/svg" viewport="0 0 60 60" xmlns:xlink="http://www.w3.org/1999/xlink">-->
                <path id="pulsar" stroke="#fff" fill="none" stroke-width="1"stroke-linejoin="round" d="M0,90L250,90Q257,60 262,87T267,95 270,88 273,92t6,35 7,-60T290,127 297,107s2,-11 10,-10 1,1 8,-10T319,95c6,4 8,-6 10,-17s2,10 9,11h210" /> -->
            </svg>
        </div>
        <div class="container-fluid">
            <div class="row">
                    <div class="col-lg-2 col-md-3 col-sm-3 hidden-xs header-div leftSidebar">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 logo-img-div">
                                <img class="img-responsive center-block logo-img" src="<?php echo SITE_PATH; ?>/images/logo.png" alt="logo img" />
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 navbar-div no-padding">
                                <ul class="navbar">
                                    <li class="nav-li<?php if(preg_match('/index.php/',$_SERVER['SCRIPT_NAME'])) { ?> active <?php } ?>"><a href="<?php echo SITE_PATH; ?>"><span><i class="fa fa-home" aria-hidden="true"></i></span>HOME</a></li>
                                    <li class="nav-li<?php if(preg_match('/about-us.php/',$_SERVER['SCRIPT_NAME'])) { ?> active <?php } ?>"><a href="<?php echo SITE_PATH; ?>/about-us.php"><span><i class="fa fa-users" aria-hidden="true"></i></span>ABOUT US</a></li>
                                    <li class="nav-li<?php if(preg_match('/live-tv.php/',$_SERVER['SCRIPT_NAME'])) { ?> active <?php } ?>"><a href="<?php echo SITE_PATH; ?>/live-tv.php"><span><i class="fa fa-youtube-play" aria-hidden="true"></i></span>LIVE TV</a></li>
                                    <li class="nav-li<?php if(preg_match('/videos.php/',$_SERVER['SCRIPT_NAME'])) { ?> active <?php } ?>"><a href="<?php echo SITE_PATH; ?>/videos.php"><span><i class="fa fa-video-camera" aria-hidden="true"></i></span>VIDEOS</a></li>
                                    <li class="nav-li<?php if(preg_match('/file-articles.php/',$_SERVER['SCRIPT_NAME'])) { ?> active <?php } ?>"><a href="<?php echo SITE_PATH; ?>/file-articles.php"><span><i class="fa fa-file-text" aria-hidden="true"></i></span>ARTICLES</a></li>
                                    <li class="nav-li<?php if(preg_match('/gallery.php/',$_SERVER['SCRIPT_NAME'])) { ?> active <?php } ?>"><a href="<?php echo SITE_PATH; ?>/gallery.php"><span><i class="fa fa-picture-o" aria-hidden="true"></i></span>GALLERY</a></li>
                                    <li class="nav-li<?php if(preg_match('/contact-us.php/',$_SERVER['SCRIPT_NAME'])) { ?> active <?php } ?>"><a href="<?php echo SITE_PATH; ?>/contact-us.php"><span><i class="fa fa-user-circle" aria-hidden="true"></i></span>CONTACT US</a></li>
                                    <li class="nav-li<?php if(preg_match('/add-with-us.php/',$_SERVER['SCRIPT_NAME'])) { ?> active <?php } ?>"><a href="<?php echo SITE_PATH; ?>/add-with-us.php"><span><i class="fa fa-buysellads" aria-hidden="true"></i></span>ADVERTISE WITH US</a></li>
                                    <li class="nav-li<?php if(preg_match('/advertise.php/',$_SERVER['SCRIPT_NAME'])) { ?> active <?php } ?>"><a href="<?php echo SITE_PATH; ?>/advertise.php"><span><i class="fa fa-bullhorn" aria-hidden="true"></i></span>ADVERTISERS</a></li>
                                </ul>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a href="#" class="apple-store-apps-data">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="app-store"><i class="fa fa-apple" aria-hidden="true"></i></span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 padding-left">
                                            <span class="apple-available-text">Available on the</span>
                                            <span class="apple-app-store-text">App Store</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a href="#" class="play-store-apps-data">
                                    <div class="row term_condition">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="play-store"><img class="img-responsive center-block" src="<?php echo SITE_PATH;?>/images/store.png" /></span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 padding-left">
                                            <span class="play-available-text">Available on the</span>
                                            <span class="play-app-store-text">Play Store</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 privacy-policy-topmargin">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 terms-anchor">
                                        <a href="#term-condition" data-toggle="modal" class="term-condition">Terms & Conditions</a>
                                        <div class="modal fade margin-box" id="term-condition" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <h2 class="modal-title" id="term-conditionTitle">Modal title</h2>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                              </div>
                                              <div class="modal-body">
                                                terms and conditions
                                              </div>
                                              <div class="modal-footer">
                                                <button type="button" class="btn-popup modal-close-btn" data-dismiss="modal">Close</button>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 privacy-anchor">
                                        <a href="#privacy-policy" data-toggle="modal" class="privacy-policy">Privacy Policy</a>
                                        <div class="modal fade margin-box" id="privacy-policy" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <h2 class="modal-title" id="privacy-policyTitle">Modal title</h2>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                              </div>
                                              <div class="modal-body">
                                                Privacy Policy
                                              </div>
                                              <div class="modal-footer">
                                                <button type="button" class="btn-popup modal-close-btn" data-dismiss="modal">Close</button>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 disclaimer-anchor">
                                        <a href="#disclaimer" data-toggle="modal" class="disclaimer">Disclaimer</a>
                                        <div class="modal fade margin-box" id="disclaimer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <h2 class="modal-title" id="disclaimerTitle">Modal title</h2>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                              </div>
                                              <div class="modal-body">
                                                Disclaimer
                                              </div>
                                              <div class="modal-footer">
                                                <button type="button" class="btn-popup modal-close-btn" data-dismiss="modal">Close</button>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 copyright-margin">
                                        <span class="copyright">&copy;&nbsp;Copyrights 2017</span>
                                        <span><a href="#" class="copyright-ace-health">ACE Health Channel</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="col-lg-10 col-md-9 col-sm-9 col-xs-12 xs-width content">
                    <div class="row">
                        <div class="hidden-lg hidden-md hidden-sm col-xs-12 logo-img-div">
                            <img class="img-responsive center-block logo-img" src="<?php echo SITE_PATH;?>/images/logo.png" alt="logo img" />
                        </div>
                        <div class=" hidden-lg hidden-md hidden-sm navbtn-fixed">
                            <div class="hidden-lg hidden-md hidden-sm col-xs-12 navbar-margin">
                                <div class="m-social-icons">
                                    <ul class="social-icons">
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                                    </ul>
                                </div>
                                <div id="demo_box" class="pull-right">
                                    <span class="pop_ctrl"><i class="fa fa-bars"></i></span>
                                    <ul id="demo_ul" class="m-navbar">
                                        <li class="demo_li<?php if(preg_match('/index.php/',$_SERVER['SCRIPT_NAME'])) { ?> mnavbar-active <?php } ?>"><a href="<?php echo SITE_PATH; ?>"><div><i class="fa fa-home"></i></div><div>HOME</div></a></li>
                                        <li class="demo_li<?php if(preg_match('/about-us.php/',$_SERVER['SCRIPT_NAME'])) { ?> mnavbar-active <?php } ?>"><a href="<?php echo SITE_PATH; ?>/about-us.php"><div><i class="fa fa-users"></i></div><div>ABOUT US</div></a></li>
                                        <li class="demo_li<?php if(preg_match('/live-tv.php/',$_SERVER['SCRIPT_NAME'])) { ?> mnavbar-active <?php } ?>"><a href="<?php echo SITE_PATH; ?>/live-tv.php"><div><i class="fa fa-youtube-play"></i></div><div>LIVE TV</div></a></li>
                                        <li class="demo_li<?php if(preg_match('/videos.php/',$_SERVER['SCRIPT_NAME'])) { ?> mnavbar-active <?php } ?>"><a href="<?php echo SITE_PATH; ?>/videos.php"><div><i class="fa fa-video-camera"></i></div><div>VIDEOS</div></a></li>
                                        <li class="demo_li<?php if(preg_match('/file-articles.php/',$_SERVER['SCRIPT_NAME'])) { ?> mnavbar-active <?php } ?>"><a href="<?php echo SITE_PATH; ?>/file-articles.php"><div><i class="fa fa-file-text"></i></div><div>ARTICLES</div></a></li>
                                        <li class="demo_li<?php if(preg_match('/gallery.php/',$_SERVER['SCRIPT_NAME'])) { ?> mnavbar-active <?php } ?>"><a href="<?php echo SITE_PATH; ?>/gallery.php"><div><i class="fa fa-picture-o"></i></div><div>GALLERY</div></a></li>
                                        <li class="demo_li<?php if(preg_match('/contact-us.php/',$_SERVER['SCRIPT_NAME'])) { ?> mnavbar-active <?php } ?>"><a href="<?php echo SITE_PATH; ?>/contact-us.php"><div><i class="fa fa-user-circle"></i></div><div>CONTACT US</div></a></li>
                                        <li class="demo_li<?php if(preg_match('/add-with-us.php/',$_SERVER['SCRIPT_NAME'])) { ?> mnavbar-active <?php } ?>"><a href="<?php echo SITE_PATH; ?>/add-with-us.php"><div><i class="fa fa-buysellads"></i></div><div>ADVERTISE WITH US</div></a></li>
                                        <li class="demo_li<?php if(preg_match('/advertise.php/',$_SERVER['SCRIPT_NAME'])) { ?> mnavbar-active <?php } ?>"><a href="<?php echo SITE_PATH; ?>/advertise.php"><div><i class="fa fa-bullhorn"></i></div><div>ADVERTISER</div></a></li>
                                        <li class="demo_li btn-dlod"><a href="#"><div><i class="fa fa-download"></i></div><div>DOWNLOAD APPS</div></a></li>
                                        <li class="back-icon">
                                            <a href="#" class="back-to-navbar"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
                                        </li>
                                        <li class="download-icons">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <a href="#" class="apple-store-apps-data no-margin">
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                            <span class="app-store pull-right"><i class="fa fa-apple" aria-hidden="true"></i></span>
                                                        </div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 padding-left">
                                                            <span class="apple-available-text text-left">Available on the</span>
                                                            <span class="apple-app-store-text text-left">App Store</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </li>
                                        <li class="download-icons">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <a href="#" class="play-store-apps-data no-margin">
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                            <span class="play-store pull-right"><img class="img-responsive center-block" src="images/store.png" /></span>
                                                        </div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 padding-left">
                                                            <span class="play-available-text text-left">Available on the</span>
                                                            <span class="play-app-store-text text-left">Play Store</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="hidden-lg hidden-md hidden-sm col-xs-12">
                                <div class="mobile-player">
                                    <audio preload="<?php echo SITE_PATH;?>/AudioPlayer/AudioPlayer/auto" controls>
                        				<source src="<?php echo SITE_PATH;?>/AudioPlayer/AudioPlayer/audio/BlueDucks_FourFlossFiveSix.mp3">
                        				<source src="<?php echo SITE_PATH;?>/AudioPlayer/AudioPlayer/audio/BlueDucks_FourFlossFiveSix.ogg">
                        				<source src="<?php echo SITE_PATH;?>/AudioPlayer/AudioPlayer/audio/BlueDucks_FourFlossFiveSix.wav">
                        			</audio>
                    		    </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-7 col-md-6 col-sm-6 hidden-xs full-padding mp3-player">
                            <img class="player-left" src="<?php echo SITE_PATH;?>/images/mright.jpg" />
                            <audio preload="<?php echo SITE_PATH; ?>/AudioPlayer/AudioPlayer/auto" controls>
                    			<source src="<?php echo SITE_PATH; ?>/AudioPlayer/AudioPlayer/audio/BlueDucks_FourFlossFiveSix.mp3">
                    			<source src="<?php echo SITE_PATH; ?>/AudioPlayer/AudioPlayer/audio/BlueDucks_FourFlossFiveSix.ogg">
                    			<source src="<?php echo SITE_PATH; ?>/AudioPlayer/AudioPlayer/audio/BlueDucks_FourFlossFiveSix.wav">
                    		</audio>
                    		<img class="player-right" src="<?php echo SITE_PATH;?>/images/mplayr.jpg" />
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs no-padding">
                            <ul class="social-icons">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                            </ul>
                        </div>
                        <div class="col-lg-2 col-md-3 col-sm-3 hidden-xs add-background">
                            <span class="advertise"><a href="#">Advertise With Us</a></span>
                        </div>
               
    
