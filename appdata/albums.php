<?php require_once('../adminace/config/config.php');?>
<?php 
    $arr2=array();
    $data=array();
    $data['title']=array();
    $q="select * from albums order by id desc";
    $r=mysqli_query($conn,$q);
    while($row=mysqli_fetch_array($r)){
        $id=$row['id'];
        $data['id']=$row['id'];
        $data['title']=$row['title'];
        if(empty($row['cover'])){
            $data['cover']="null";
        }else{
            $data['cover']="https://ace-giftechies818.c9users.io/images/albumcover/".$row['cover'];
        }
        $q1="select * from albumimages where parent_id='$id'";
        $r1=mysqli_query($conn,$q1);
        $sr=0;
        $data['image']=array();
        $newarray=array();
        while($row1=mysqli_fetch_array($r1)){
            $newarray['images']="https://ace-giftechies818.c9users.io/images/albumimages/".$row1['image'];
            array_push($data['image'],$newarray);
        }
        if(mysqli_num_rows($r1)==0){
            $newarray['images']="https://ace-giftechies818.c9users.io/images/albumcover/".$row['cover'];
        }
        array_push($data['image'],$newarray);
        array_push($arr2,$data);
    }
    if(empty($arr2)){
        $arr2="null";
    }
    echo json_encode(array("data"=>$arr2), JSON_UNESCAPED_SLASHES);