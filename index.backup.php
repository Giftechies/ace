<?php include_once 'common-files/header.php'; ?>
        <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 no-padding-right">
                    <div class="update-shape">
                        <div class="news-updates text-center">UPDATES</div>
                        <div id="triangle-right"></div>
                        <div id="new-arrow"></div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 no-padding-left">
                    <div class="scroll-left">
                        <p>Characters that are not present on your keyboard can also be replaced by entities.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-7 col-md-8 col-sm-12 col-xs-12 video-padding">
            <a class="fancybox" data-fancybox-type="iframe" rel="myGallery" href="https://www.youtube.com/embed/gF0rrpMH-Jo">
               <iframe class="embed-responsive-item" width="100%" height="380" src="https://www.youtube.com/embed/gF0rrpMH-Jo" frameborder="0" allowfullscreen></iframe>
           </a>        
        </div>
        <div class="col-lg-5 col-md-4 col-sm-12 col-xs-12 padding">
            <div class="row">
                <div class="lg-12 col-md-12 col-sm-6 col-xs-12 health-img1 width-function">
                        <div class="health-img-div1">
                            <a class="fancybox" rel="myGallery" href="images/health1.jpg">
                                <img class="img-responsive center-block" src="images/health1.jpg" />
                            </a>
                            <span class="health-care-title1 text-center">DISCUSSION ABOUT HEALTH</span>
                        </div>
                    </div>
                <div class="lg-12 col-md-12 col-sm-6 col-xs-12 health-img2 width-function">
                    <div class="health-img-div2">
                        <a class="fancybox" rel="myGallery" href="images/health2.jpg">
                            <img class="img-responsive center-block" src="images/health2.jpg" />
                        </a>
                        <span class="health-care-title2 text-center">HEALTH DISCUSSION WITH DR. BEANT</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="hidden-lg hidden-md col-sm-12 col-xs-12 exclusive-top-padding">
            <div class="schedule-text5">
                <div class="exclusive-border center-block"></div>
                <div class="inner"><span class="exclusive-text">EXCLUSIVE</span></div>
                <h1 class="ace-health-progrm">Ace Health Programs</h1>
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 today-text-padding">
            <div class="schedule-text1">
                <span class="inner today-font">Today`s</span>
                <span class="inner today-font"> Schedule</span>
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 today-text-padding">
            <div class="schedule-text2">
                <span class="inner inner-padding-left">11:00 AM</span>
                <span class="inner">KNOWLEDGE</span>
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 today-text-padding">
            <div class="schedule-text3">
                <span class="inner inner-padding-left">11:30 AM</span>
                <span class="inner">WORKING</span>
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 today-text-padding">
            <div class="schedule-text4">
                <span class="inner inner-padding-left">12:00 PM</span>
                <span class="inner">STRONG</span>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 hidden-sm hidden-xs exclusive-health-padding">
            <div class="schedule-text5">
                <div class="exclusive-border center-block"></div>
                <div class="inner"><span class="exclusive-text">EXCLUSIVE</span></div>
                <h1 class="ace-health-progrm">Ace Health Programs</h1>
            </div>
        </div>
    </div>
    
    <div class="row margin-timeline-videos">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="border-bottom">
                        <h2 class="latest-articles">LATEST ARTICLES</h2>
                        <hr class="hr-border1">
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 max-height-news">
                    <ul class="timeline">
                        <?php $q1="select * from articles order by id desc limit 3";
                              $run=mysqli_query($conn,$q1);
                              while($row=mysqli_fetch_array($run)){
                                  $date=new DateTime($row['created']);
                        ?>
                                <li class="timeline-inverted">
                                    <div class="news-schedules pull-left">
                                        <span class="news-date"><?php echo date_format($date, 'd-m-Y');?></span>
                                        <span class="news-day"><?php echo date_format($date, 'D');?></span>
                                    </div>
                                    <div class="timeline-badge"><i class="glyphicon glyphicon-credit-card"></i></div>
                                    <div class="timeline-panel">
                                        <?php if(!empty($row['image'])){?>
                                        <div class="timeline-img pull-left">
                                            <img class="img-responsive" src="<?php echo SITE_PATH;?>/images/articalimages/<?php echo $row['image'];?>" alt="news img" />
                                        </div>
                                        <div class="timeline-news">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title"><a href="<?php echo SITE_PATH.'/article.php?id='.$row['id'];?>"><?php echo strlen($row['title']) > 20 ? substr($row['title'],0,20)."..." : $row['title'];?></a></h4>
                                                <div class="m-news-schedules hidden-lg hidden-md hidden-sm">
                                                    <span class="m-news-date-day"><?php echo date_format($date, 'd-m-Y');?> <span>(<?php echo date_format($date, 'D');?>)</span></span>
                                                </div>
                                            </div>
                                            <div class="timeline-body">
                                                <?php echo strlen($row['content']) > 120 ? substr($row['content'],0,120)."..." : $row['content'];?>
                                            </div>
                                        </div>
                                        <?php }else{?>
                                            <div class="timeline-news" style="width:100%">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title"><a href="<?php echo SITE_PATH.'/article.php?id='.$row['id'];?>"><?php echo strlen($row['title']) > 20 ? substr($row['title'],0,20)."..." : $row['title'];?></a></h4>
                                                <div class="m-news-schedules hidden-lg hidden-md hidden-sm">
                                                    <span class="m-news-date-day"><?php echo date_format($date, 'd-m-Y');?> <span>(<?php echo date_format($date, 'D');?>)</span></span>
                                                </div>
                                            </div>
                                            <div class="timeline-body">
                                                <?php echo strlen($row['content']) > 200 ? substr($row['content'],0,200)."..." : $row['content'];?>
                                            </div>
                                        </div>
                                        <?php }?>
                                    </div>
                                </li>  
                        <?php   }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 videos-back-color">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="border-bottom">
                        <h2 class="latest-videos">LATEST VIDEOS</h2>
                        <hr class="hr-border2">
                    </div>
                </div>
                <?php 
                    $q="select * from videos order by 1 desc limit 3";
                    $run=mysqli_query($conn,$q);
                    while($row=mysqli_fetch_array($run)){
                        $title=$row['title'];
                        $videolink=$row['url'];
                        $ytarray=explode("/", $videolink);
                        $ytendstring=end($ytarray);
                        $ytendarray=explode("?v=", $ytendstring);
                        $ytendstring=end($ytendarray);
                        $ytendarray=explode("&", $ytendstring);
                        $ytcode=$ytendarray[0];
                        $url= "https://www.youtube.com/embed/".$ytcode;
                  ?>  
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 latest-videos-margin1">
                    <div class="embeded-videos">
                        <iframe class="embed-responsive-item" width="100%" height="125" src="<?php echo $url;?>" frameborder="0" allowfullscreen></iframe>
                        <span class="video-title"><i class="fa fa-play pull-left" aria-hidden="true"></i><?php echo $title;?></span>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 sopnsr-padding">
            <span class="our-sposnors">OUR SPOSNORS</span>
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
            <ul class="bxslider">
               <li><a href="#"><img src="/images/slider-logo1.jpg" alt="alider-logo" /></a></li>
               <li><a href="#"><img src="/images/slider-logo2.jpg" alt="alider-logo" /></a></li>
               <li><a href="#"><img src="/images/slider-logo3.jpg" alt="alider-logo" /></a></li>
               <li><a href="#"><img src="/images/slider-logo4.jpg" alt="alider-logo" /></a></li>
               <li><a href="#"><img src="/images/slider-logo5.jpg" alt="alider-logo" /></a></li>
               <li><a href="#"><img src="/images/slider-logo6.jpg" alt="alider-logo" /></a></li>
               <li><a href="#"><img src="/images/slider-logo7.jpg" alt="alider-logo" /></a></li>
               <li><a href="#"><img src="/images/slider-logo1.jpg" alt="alider-logo" /></a></li>
               <li><a href="#"><img src="/images/slider-logo2.jpg" alt="alider-logo" /></a></li>
               <li><a href="#"><img src="/images/slider-logo3.jpg" alt="alider-logo" /></a></li>
               <li><a href="#"><img src="/images/slider-logo4.jpg" alt="alider-logo" /></a></li>
               <li><a href="#"><img src="/images/slider-logo5.jpg" alt="alider-logo" /></a></li>
            </ul>
        </div>
    </div>
<?php include_once 'common-files/footer.php'; ?>