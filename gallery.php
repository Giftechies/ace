<?php include_once 'common-files/header.php'; ?>
        <link rel="stylesheet" href="<?php echo SITE_PATH;?>/css/loader.css" />
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 image-banner-img">
            <div class="banner-blur-background"></div>
            <h1 class="image-heading">Gallery</h1>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 albums-navbar">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" class="all-images" href="#items">All Images</a></li>
                <li><a data-toggle="tab" href="#albumitems" class="all-albums">Albums</a></li>
            </ul>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 tab-content">
            <div class="tab-pane fade in active" id="items" image="images">
                    <input type="hidden" name="hideimage" class="rowimage" value="imageval">
                    <input type="hidden" name="items" class="divitem" value="items">
                <?php 
                     $q1="select * from albumimages order by 1 desc LIMIT 0,8"; 
                      $run1=mysqli_query($conn,$q1);
                      while($row1=mysqli_fetch_array($run1)){
                       $last_id=$row1['id'];
                    ?>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 album-padding">
                        <div class="gallery-images">
                                <a class="fancybox" href="<?php echo SITE_PATH;?>/images/albumimages/<?php echo $row1['image'];?>" data-fancybox-group="gallery" >
                                    <div class="view second-effect">
                                        <img class="img-responsive center-block" src="<?php echo SITE_PATH;?>/images/albumimagesthumbs/<?php echo $row1['image'];?>" alt="Image" title="Image" />
                                        <div class="mask">
                                            <i class="fa fa-search-plus info" aria-hidden="true"></i>
                                    	</div>
                                	</div>
                                </a>
                            <span class="gallery-image-title"> Title </span>
                        </div>
                    </div>
                    
                <?php } ?>
                <input type="hidden" name="last_id" class="last" value="<?php echo $last_id; ?>">
                
                <div class='row loadmore'><div class='loader' id='loader'></div></div>
            </div>
            <div class="tab-pane fade" id="albumitems" image="albums">
                <input type="hidden" name="hidealbum" class="rowalbum" value="">
                 <input type="hidden" name="albumitems" class="divalbum" value="albumitems">
                 <div class="albumitemsnew">
                <?php 
                     $q1="select * from albums order by 1 desc LIMIT 0,8"; 
                      $run1=mysqli_query($conn,$q1);
                      while($row1=mysqli_fetch_array($run1)){
                          $last_id = $row1['id']; 
                        $id=$row1['id'];
                        $title=$row1['title'];
                        $cover=$row1['cover'];
                        $q="select count(id) FROM albumimages WHERE parent_id='$id'";
                        $countquery=mysqli_query($conn,$q);
                        $countrow = mysqli_fetch_row($countquery);
                        $count = $countrow[0];
                    ?>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 album-padding albumhide">
                        <div class="gallery-images album-hover">
                            <a href="#" class="albums-anchor-tag albumclick" id="<?php echo $id;?>" onclick="aimage(id)" filtervalue="<?php echo $id;?>">
                                    <img class="img-responsive center-block" src="<?php echo SITE_PATH;?>/images/albumthumbs/<?php echo $cover;?>" alt="<?php echo $title." Image"; ?>" title="Image" />
                                
                                <div class="album-hover-icon">
                                   <div class="album-hover-icon-folder"></div>
                               </div> 
                            </a>
                            <span class="gallery-image-title"><?php echo ucwords($title); ?> (<?php echo $count; ?>)</span>
                        </div>
                    </div>
                    
                    <input type="hidden" name="last_id" class="lastalbum" value="<?php echo $last_id; ?>">
                <?php } ?>
                </div>
                     <div class="clearfix"></div>
                     <div class='row loadmore'><div class='loader' id='loader'></div></div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="onealbum">
                        <div class="returnimages" id="returnimages">
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
 
<?php include_once 'common-files/footer.php'; ?>
<script src="<?php echo SITE_PATH;?>/js/loadmore.js"></script>
