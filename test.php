<?php require_once('adminace/config/config.php');?>
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
<style type="text/css">
html, body {
  height: 100%;
  width: 100%;
  margin: 0;
  padding: 0;
}

#map_canvas {
  height: 100%;
  width: 80%;
}

#side_bar {
  height: 100%;
  width: 20%;
  overflow: auto;
}

@media print {
  html, body {
    height: auto;
  }

  #map_canvas {
    height: 650px;
  }
}
#search-panel {
  position: absolute;
  top: 5px;
  left: 50%;
  margin-left: -180px;
  width: 350px;
  z-index: 5;
  background-color: #fff;
  padding: 5px;
  border: 1px solid #999;
}
#target {
  width: 345px;
}
</style>
    <title>Google Maps JavaScript API v3 Example: SearchBox</title>
    <script src="https://maps.googleapis.com/maps/api/js?v=3&amp;sensor=false&amp;libraries=places"></script>
    <script src="<?php echo SITE_PATH;?>/js/map.js"></script>
  </head>
  <body>
    <div id="search-panel">
      <input id="target" type="text" placeholder="Search Box">
    </div>
<table border="1" style="width:100%; height: 100%;"><tr><td style="width:80%;height:100%;">
    <div id="map_canvas" style="width:100%; height: 100%;"></div>
</td><td style="width:20%;height:100%;">
    <div id="side_bar"></div>
</td></tr></table>
  </body>
</html>
