<?php include_once 'common-files/header.php'; ?>
<?php require_once('common-files/functions.php');?>
        
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about-us-banner-img">
        <div class="banner-blur-background"></div>
        <h1 class="about-us-heading">Articles</h1>
    </div>
        <div class="col-lg-12 col-md-12 col-sm-12 videosection">
            <div class="row">
                <?php 
                  if(isset($_GET["page"])){ //Get page number from $_GET["page"]
                        $page_number = filter_var($_GET["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
                        if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
                    }else{
                        $page_number = 1; //if there's no page number, set it to 1
                    }
                    $q="select * from articles order by 1 desc";
                      $run=mysqli_query($conn,$q);
                      $item_per_page=12;
                      $page_position = (($page_number-1) * $item_per_page);
                      $get_total_rows=mysqli_num_rows($run);
                      $total_pages = ceil($get_total_rows/$item_per_page);
                     $q1="select * from articles order by 1 desc LIMIT $page_position, $item_per_page"; 
                      $run1=mysqli_query($conn,$q1);
                      while($row=mysqli_fetch_array($run1)){
                        $date=new DateTime($row['created']);
                         ?>
                    <div class="col-sm-4">
                        <div class="timeline-panel articles-panel">
                            <?php if(!empty($row['image'])){?>
                            <div class="timeline-img pull-left">
                                <img class="img-responsive" src="<?php echo SITE_PATH;?>/images/articalimages/<?php echo $row['image'];?>" alt="news img" />
                            </div>
                            <div class="timeline-news">
                                <div class="timeline-heading">
                                    <h4 class="timeline-title"><a href="<?php echo SITE_PATH.'/article.php?id='.$row['id'];?>" id="<?php echo $row['id'];?>"><?php echo $row['title'];?></a></h4>
                                    <div class="m-news-schedules hidden-lg hidden-md hidden-sm">
                                        <span class="m-news-date-day"><?php echo date_format($date, 'd-m-Y');?> <span>(<?php echo date_format($date, 'D');?>)</span></span>
                                    </div>
                                </div>
                                <div class="timeline-body">
                                    <?php echo strlen($row['content']) > 120 ? substr($row['content'],0,120)."..." : $row['content'];?>
                                </div>
                            </div>
                            <?php }else{?>
                                <div class="timeline-news" style="width:100%">
                                <div class="timeline-heading">
                                    <h4 class="timeline-title"><a href="<?php echo SITE_PATH.'/article.php?id='.$row['id'];?>" id="<?php echo $row['id'];?>"><?php echo $row['title'];?></a></h4>
                                    <div class="m-news-schedules hidden-lg hidden-md hidden-sm">
                                        <span class="m-news-date-day"><?php echo date_format($date, 'd-m-Y');?> <span>(<?php echo date_format($date, 'D');?>)</span></span>
                                    </div>
                                </div>
                                <div class="timeline-body">
                                    <?php echo strlen($row['content']) > 120 ? substr($row['content'],0,120)."..." : $row['content'];?>
                                </div>
                            </div>
                            <?php }?>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <?php echo '<div class="row text-center">';
                    // We call the pagination function here. 
                    echo paginate($item_per_page, $page_number, $get_total_rows[0], $total_pages, $page_url);
                    echo '</div>';
            ?>
        </div>
        
        <div id="articlesmodal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Read Article</h4>
                    </div>
                    <div class="modal-body" id="getarticle">
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
                    </div>
                </div>
            </div>
        </div>
                 
    </div>
    
<?php include_once 'common-files/footer.php'; ?>
