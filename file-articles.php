<?php include_once 'common-files/header.php'; ?>
<?php require_once('common-files/functions.php');?>
</div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 articles-banner-img">
            <div class="banner-blur-background"></div>
            <h1 class="articles-heading">Articles</h1>
        </div>
    </div>

    <div class="row alll-articl-bottom-padding">
        <?php 
          if(isset($_GET["page"])){ //Get page number from $_GET["page"]
                $page_number = filter_var($_GET["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
                if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
            }else{
                $page_number = 1; //if there's no page number, set it to 1
            }
            $q="select * from articles order by 1 desc";
              $run=mysqli_query($conn,$q);
              $item_per_page=12;
              $page_position = (($page_number-1) * $item_per_page);
              $get_total_rows=mysqli_num_rows($run);
              $total_pages = ceil($get_total_rows/$item_per_page);
             $q1="select * from articles order by 1 desc LIMIT $page_position, $item_per_page"; 
              $run1=mysqli_query($conn,$q1);
              while($row=mysqli_fetch_array($run1)){
                 $date=new DateTime($row['created']);
                 ?>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
                <div class="bottom-brdr1">
                    <div class="row article-padding-bottom">
                        <?php if(!empty($row['image'])){?>
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                            <img class="img-responsive center-block" src="<?php echo SITE_PATH;?>/images/articalthumbs/<?php echo $row['image'];?>" alt="Article Image" title="<?php echo $row['title'];?>" />
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                            <a href="<?php echo SITE_PATH.'/article.php?id='.$row['id'];?>" class="article-news-title">
                                <?php echo strlen($row['title']) > 75 ? substr($row['title'],0,75)."..." : $row['title'];?>
                            </a>
                            <span class="articles-dates"><?php echo date_format($date, 'd-m-Y');?></span>
                        </div>
                        <?php }else{ ?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <a href="<?php echo SITE_PATH.'/article.php?id='.$row['id'];?>" class="article-news-title">
                                <?php echo strlen($row['title']) > 150 ? substr($row['title'],0,150)."..." : $row['title'];?>
                            </a>
                            <span class="articles-dates"><?php echo date_format($date, 'd-m-Y');?></span>
                            <?php echo strlen($row['content']) > 50 ? substr($row['content'],0,50)."..." : $row['content'];?>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php } ?>
        
    </div><!-- end row -->
    
     <?php echo '<div class="col-sm-12 text-right">';
                // We call the pagination function here. 
                echo paginate($item_per_page, $page_number, $get_total_rows[0], $total_pages, $page_url);
                echo '</div>';
        ?>
<?php include_once 'common-files/footer.php'; ?>