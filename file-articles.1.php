<?php include_once 'common-files/header.php'; ?>
</div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 articles-banner-img">
            <div class="banner-blur-background"></div>
            <h1 class="articles-heading">Articles</h1>
        </div>
    </div>

    <div class="row alll-articl-bottom-padding">
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr1">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr2">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr3">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr1">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr2">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr3">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr1">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr2">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr3">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr1">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr2">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr3">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr1">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr2">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr3">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr1">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr2">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr3">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr1">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr2">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr3">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr1">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr2">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr3">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr1">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr2">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr3">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr1">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr2">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr3">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr1">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr2">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr3">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr1">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr2">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 articles-padding mobile-full-article">
            <div class="bottom-brdr3">
                <div class="row article-padding-bottom">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mobile-article-img">
                        <img class="img-responsive center-block" src="images/news.jpg" alt="article1" title="article title" />
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <a href="#" class="article-news-title">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                        </a>
                        <span class="articles-dates">20/03/2017</span>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- end row -->
<?php include_once 'common-files/footer.php'; ?>