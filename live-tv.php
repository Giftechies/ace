<?php include_once 'common-files/header.php'; ?>
</div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live-tv-banner-img">
            <div class="banner-blur-background"></div>
            <h1 class="live-tv-heading">Live TV</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="live-tv-video">
                 <iframe height="350" width="100%" src="https://www.youtube.com/embed/gF0rrpMH-Jo" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
        <div class="col-lg-offset-0 col-lg-4 col-md-offset-0 col-md-4 col-sm-offset-3 col-sm-6 col-xs-12 live-tv-img1">
           <div id="myCarousel" class="carousel slide first-slider" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                          <li data-target="#myCarousel" data-slide-to="1"></li>
                          <li data-target="#myCarousel" data-slide-to="2"></li>
                          <li data-target="#myCarousel" data-slide-to="3"></li>
                        </ol>
                    
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner vertical-slider" role="listbox">
                          <div class="item active">
                            <img src="images/live-tv2.jpg" alt="Chania" width="460" height="345">
                          </div>
                    
                          <div class="item">
                            <img src="images/live-tv2.jpg" alt="Chania" width="460" height="345">
                          </div>
                        
                          <div class="item">
                            <img src="images/live-tv2.jpg" alt="Flower" width="460" height="345">
                          </div>
                    
                          <div class="item">
                            <img src="images/live-tv2.jpg" alt="Flower" width="460" height="345">
                          </div>
                        </div>
                    
                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live-tv-img2">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                          <li data-target="#myCarousel" data-slide-to="1"></li>
                          <li data-target="#myCarousel" data-slide-to="2"></li>
                          <li data-target="#myCarousel" data-slide-to="3"></li>
                        </ol>
                    
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                          <div class="item active">
                            <img src="images/live-tv3.jpg" alt="Chania" width="460" height="345">
                          </div>
                    
                          <div class="item">
                            <img src="images/live-tv1.jpg" alt="Chania" width="460" height="345">
                          </div>
                        
                          <div class="item">
                            <img src="images/live-tv3.jpg" alt="Flower" width="460" height="345">
                          </div>
                    
                          <div class="item">
                            <img src="images/live-tv1.jpg" alt="Flower" width="460" height="345">
                          </div>
                        </div>
                    
                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>
                   <!-- <img src="images/live-tv3.jpg" alt="live tv img2" title="live tv2">-->
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live-tv-img3 m-view-padding">
                   <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                          <li data-target="#myCarousel" data-slide-to="1"></li>
                          <li data-target="#myCarousel" data-slide-to="2"></li>
                          <li data-target="#myCarousel" data-slide-to="3"></li>
                        </ol>
                    
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                          <div class="item active">
                            <img src="images/live-tv3.jpg" alt="Chania" width="460" height="345">
                          </div>
                    
                          <div class="item">
                            <img src="images/live-tv1.jpg" alt="Chania" width="460" height="345">
                          </div>
                        
                          <div class="item">
                            <img src="images/live-tv3.jpg" alt="Flower" width="460" height="345">
                          </div>
                    
                          <div class="item">
                            <img src="images/live-tv1.jpg" alt="Flower" width="460" height="345">
                          </div>
                        </div>
                    
                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>
                </div>
            </div>
        </div>
    </div><!--row end-->
<?php include_once 'common-files/footer.php'; ?>