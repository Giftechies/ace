<?php include_once 'common-files/header.php'; ?>
<link rel="stylesheet" href="<?php echo SITE_PATH;?>/css/loader.css" />
</div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 videos-banner-img">
        <div class="banner-blur-background"></div>
        <h1 class="videos-heading">Videos</h1>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 no-padding" id="items">
        <?php 
            
              $q1="select * from videos order by 1 desc LIMIT 0,9"; 
              $run1=mysqli_query($conn,$q1);
              while($row=mysqli_fetch_array($run1)){
                    $last_id =$row['id'];
                    $id=$row['id'];
                    $title=$row['title'];
                    $videolink=$row['url'];
                    $ytarray=explode("/", $videolink);
                    $ytendstring=end($ytarray);
                    $ytendarray=explode("?v=", $ytendstring);
                    $ytendstring=end($ytendarray);
                    $ytendarray=explode("&", $ytendstring);
                    $ytcode=$ytendarray[0];
                    $url= "https://www.youtube.com/embed/".$ytcode;
                 ?>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 videos-padding">
                    <a class="various iframe" href="<?php echo $url; ?>?rel=0&autoplay=1">
                        <div class="videos-div embed-responsive embed-responsive-4by3">
                           <iframe class="embed-responsive-item" src="<?php echo $url; ?>" frameborder="0" allowfullscreen></iframe>
                            <div class="video-iframe"></div>
                        <span class="gallery-video-title"><?php echo $title;?></span>
                        </div>
                    </a>
                    
                </div>
           
        <?php } ?>
        <script type="text/javascript">var last_id = "<?php echo $last_id; ?>";</script>
        <div class='loadmore'><div class='loader' id='loader'></div></div>
   </div>
</div>
<?php include_once 'common-files/footer.php'; ?>
<script src="<?php echo SITE_PATH;?>/js/videoloader.js"></script>
