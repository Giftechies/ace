<?php include_once 'common-files/header.php'; ?>
</div>

<section class="postwrapper clearfix">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 contact-us-banner-img">
            <div class="banner-blur-background"></div>
            <h1 class="contact-us-heading">Contact Us</h1>
        </div>
        
        <?php $q="select * from contacts order by 1 desc limit 1";
                $run=mysqli_query($conn,$q);
                while($row=mysqli_fetch_array($run)){
                    $address=$row['address'];
                    $mobile=$row['mobile'];
                    $phone=$row['phone'];
                    $email=$row['email'];
                    $country=$row['country'];
                	$state=$row['state'];
                	$city=$row['city'];
                  	$p1="select * from countries where id=$country";
                  	$run1=mysqli_query($conn,$p1);
                    while($row1=mysqli_fetch_array($run1)){
                      $countryname=$row1['name'];
                    }
                    $p2="select * from states where id=$state";
                  	$run2=mysqli_query($conn,$p2);
                    while($row2=mysqli_fetch_array($run2)){
                      $statename=$row2['name'];
                    }
                    $p3="select * from cities where id=$city";
                  	$run3=mysqli_query($conn,$p3);
                    while($row3=mysqli_fetch_array($run3)){
                      $cityname=$row3['name'];
                    }
                	
                	$pin=$row['pin'];
                }
        ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 contact-icons-padding">
            <div class="contact_details">
                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                    <div class="address-fields text-center" data-effect="helix">
                        <div class="address-icons" id="hexagon"><i class="fa fa-map-marker fa-2x"></i></div>
                        <div class="contact-us-address-title"><h3>Address</h3></div>
                        <p class="contact-us-details"><?php echo $address; ?><br>
                        <?php echo $cityname.", ".$statename.", ".$countryname." ".$pin; ?></p>
                    </div>
                </div><!-- end col-lg-4 -->
                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                    <div class="address-fields text-center" data-effect="helix">
                        <div class="address-icons" id="hexagon"><i class="fa fa-mobile-phone fa-2x"></i></div>
                        <div class="contact-us-address-title"><h3>Mobile Number</h3></div>
                        <p class="contact-us-details"><strong>Mobile:</strong> 
                        <a href="tel:<?php echo $mobile; ?>"><?php echo $mobile; ?></a><br>
                        <strong>Phone:</strong> <a href="tel:<?php echo $phone; ?>"> <?php echo $phone; ?></a></p>
                    </div>
                </div><!-- end col-lg-4 -->
                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                    <div class="address-fields text-center" data-effect="helix">
                        <div class="address-icons" id="hexagon"><i class="fa fa-envelope fa-2x"></i></div>
                        <div class="contact-us-address-title"><h3>Email</h3></div>
                        <p class="contact-us-details"><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a><br>
                        <strong>Skype:</strong> YourTagline
                        </p>
                    </div>
                </div><!-- end col-lg-4 -->
            </div><!-- end contact_details -->
            <div class="clearfix"></div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 form-padding">
            <div class="drop-us-a-line text-center">
            	<h1>DROP US A LINE</h1>
            	<hr>
            </div>
            <div class="clearfix"></div>
			<form id="contactform" action="#" name="contactform" method="post" data-effect="slide-bottom">
            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                	<input type="text" name="name" id="name" class="form-control name" placeholder="Name"> 
                </div>
            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                	<input type="text" name="email" id="email" class="form-control email" placeholder="Email Address"> 
                </div>
            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                	<input type="text" name="title" id="title" class="form-control title" placeholder="Title"> 
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                	<input type="text" name="subject" id="subject" class="form-control subject" placeholder="Subject"> 
                </div>
                
                <div class="clearfix"></div>
                
            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                	<textarea class="form-control Message" name="comments" id="message" rows="6" placeholder="Message"></textarea>
                </div>
                
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
					<button type="submit" value="SEND" id="submit" class="btn btn-lg btn-primary">SEND</button>
				</div>
			</form>      
                   
        </div><!--form end-->
        <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs">
            <div class="contact-map">
                <div id="search-panel hidden">
                  <input id="target" type="hidden" value="<?php echo $address;?>" placeholder="Search Box">
                </div>
                <table border="1" style="width:100%; height: 100%;">
                    <tr>
                        <td style="width:80%;height:100%;">
                            <div id="map_canvas"></div>
                        </td>
                        <td class="mapsidebar">
                            <div id="side_bar"></div>
                        </td>
                    </tr>
                </table>
            </div>
        </div><!--map div end-->
    </div><!-- end row -->
</section><!-- end postwrapper -->
<?php include_once 'common-files/footer.php'; ?>